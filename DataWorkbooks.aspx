﻿<%@ Page Title="Data Work books" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="DataWorkbooks.aspx.cs" Inherits="DataWorkbooks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>

    <script>

        $(function () {

            $("#monDocs").accordion();

        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">


    <h2>Data Workbooks Documents</h2>
    <span class="backLink">
        <asp:LinkButton ID="lnkBack" Font-Bold="true" Text="Back" runat="server" OnClick="lnkBack_Click" /></span>

    <div>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="600px"
            DataKeyNames="StateWorkbookDocID" CssClass="etacTbl" Style="margin-left: 8px;">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Uploaded file                    
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%--<a target="_blank" href='DataWorkbooksDocs\<%# Eval("PhysicalFileName")%>'>
                            <%# Eval("OriginalFileName")%></a>--%>
                        
                           <a target="_blank"  href='<%# String.Format("DataWorkbooksDownload.aspx?PhysicalFileName={0}&StateWorkbookDocID={1}",Eval("PhysicalFileName"),Eval("StateWorkbookDocID"))%>'>
                                    <%# Eval("OriginalFileName")%></a>
                    </ItemTemplate>
                    <HeaderStyle Width="550px" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton20" runat="server" Text="Delete" OnClientClick="return confirm('Are you certain you want to delete?');"
                            OnClick="LinkButton20_Click" CommandArgument='<%# Eval("StateWorkbookDocID")%>' />
                    </ItemTemplate>
                    <HeaderStyle Width="50px" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <div style="padding-left: 8px">
        <%-- <h3>Upload Data Workbooks</h3>--%>
        <asp:FileUpload ID="FU1MonEvaluation" runat="server" />
        <asp:Button ID="btnFU1MonEvaluation" runat="server" Text="Upload" ValidationGroup="Submit" OnClick="btnFU1MonEvaluation_Click" />
        <asp:Button ID="btnDataWork" runat="server" OnClick="btnDataWork_Click" Text="Download" />

        <asp:RegularExpressionValidator ID="regex1" runat="server"
            ControlToValidate="FU1MonEvaluation" ErrorMessage="Only .jpg .pdf .docx .xlsx .gif .png .jpeg .doc .ppt files are allowed"
            ValidationExpression="(.*?)\.(jpg|pdf|docx|xlsx|gif|png|jpeg|doc|xls|PDF|PPT|ppt|pptx)$" ForeColor="Red" ValidationGroup="Submit">
        </asp:RegularExpressionValidator><br />
        <asp:Label runat="server" ID="StatusLabel" ForeColor="Red" />

    </div>
</asp:Content>

