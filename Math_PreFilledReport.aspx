﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Math_PreFilledReport.aspx.cs" Inherits="Math_PreFilledReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Math PreFilled Grades Data</h2>
    
    <asp:Label ID="divMessage" runat="server" ForeColor="Red"></asp:Label>
    <div id="EnrData" runat="server">
        <p style="text-align: right; font-size: 18px">
            <asp:LinkButton ForeColor="Blue" ID="lnkData" Text="Download Math Data" runat="server" OnClick="lnkData_Click" />
        </p>

       <asp:GridView ID="grdVwList" runat="server" CssClass="gridTbl" AutoGenerateColumns="False">
                <Columns>

                    <asp:BoundField DataField="Year" HeaderText="Year" DataFormatString="{0:g}">
                        <HeaderStyle Width="100px"></HeaderStyle>
                    </asp:BoundField>

                    <asp:BoundField DataField="StateName" HeaderText="State Name" DataFormatString="{0:g}">
                        <HeaderStyle Width="250px"></HeaderStyle>
                    </asp:BoundField>

                    <asp:BoundField DataField="Grade 3" HeaderText="Grade 3" DataFormatString="{0:g}">
                        <HeaderStyle Width="50px"></HeaderStyle>
                    </asp:BoundField>

                    <asp:BoundField DataField="Grade 4" HeaderText="Grade 4" DataFormatString="{0:g}">
                        <HeaderStyle Width="50px"></HeaderStyle>
                    </asp:BoundField>
                     <asp:BoundField DataField="Grade 5" HeaderText="Grade 5" DataFormatString="{0:g}">
                        <HeaderStyle Width="50px"></HeaderStyle>
                    </asp:BoundField>

                    <asp:BoundField DataField="Grade 6" HeaderText="Grade 6" DataFormatString="{0:g}">
                        <HeaderStyle Width="50px"></HeaderStyle>
                    </asp:BoundField>
                     <asp:BoundField DataField="Grade 7" HeaderText="Grade 7" DataFormatString="{0:g}">
                        <HeaderStyle Width="50px"></HeaderStyle>
                    </asp:BoundField>

                    <asp:BoundField DataField="Grade 8" HeaderText="Grade 8" DataFormatString="{0:g}">
                        <HeaderStyle Width="50px"></HeaderStyle>
                    </asp:BoundField>

                </Columns>
            </asp:GridView>
    </div>
</asp:Content>

