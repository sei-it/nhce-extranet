﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PreFilledReport.aspx.cs" Inherits="PreFilledReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div>
        <h2>PRE-FILLED REPORT</h2>

        <h3>
            <asp:Label ID="lblpreReportHeading" runat="server"></asp:Label></h3>
        <p>
            <asp:Label ID="preReport" runat="server"></asp:Label>
        </p>
        <div>
            <h4>Select Year(s)</h4>
            <p style="align-items: center">
                <%-- <asp:CheckBoxList ID="chkSchoolyear" RepeatDirection="Horizontal" runat="server"></asp:CheckBoxList>--%>

                <asp:RadioButtonList ID="chkSchoolyear" runat="server" RepeatDirection="horizontal" Height="15px" Width="20px">
                </asp:RadioButtonList>

            </p>

            <h4>Select State</h4>
            <p>
                <asp:DropDownList ID="ddlState" Visible="true" runat="server"></asp:DropDownList>
            </p>
            <h4 style="display: none">Select Student Count Type</h4>
            <p>
           


            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
            <asp:Button ID="btnClear" runat="server" Text="Clear Search" OnClick="btnClear_Click" />
                <asp:Button ID="btnGetGrades" runat="server" Text="By Grades" OnClick="btnGetGrades_Click" />
        </div>
        <h2>
            <asp:Label ID="lblheading" Visible="false" runat="server"></asp:Label></h2>
        <asp:Label ID="divMessage" runat="server" ForeColor="Red"></asp:Label>
        <div>
            <p style="text-align: right; font-size: 18px">
                <asp:LinkButton ForeColor="Blue" ID="lnkData" Text="Download" runat="server" OnClick="lnkData_Click" />
            </p>
            <asp:PlaceHolder ID="PlaceHolder1" Visible="false" runat="server"></asp:PlaceHolder>
            <asp:GridView ID="grdVwList" runat="server" CssClass="gridTbl" AutoGenerateColumns="False">
                <Columns>

                    <asp:BoundField DataField="Year" HeaderText="Year" DataFormatString="{0:g}">
                        <HeaderStyle Width="100px"></HeaderStyle>
                    </asp:BoundField>

                    <asp:BoundField DataField="StateName" HeaderText="State Name" DataFormatString="{0:g}">
                        <HeaderStyle Width="350px"></HeaderStyle>
                    </asp:BoundField>

                    <asp:BoundField DataField="PercentOverall" HeaderText="Percent Overall" DataFormatString="{0:g}">
                        <HeaderStyle Width="350px"></HeaderStyle>
                    </asp:BoundField>

                    <asp:BoundField DataField="Quartile" HeaderText="Quartile" DataFormatString="{0:g}">
                        <HeaderStyle Width="200px"></HeaderStyle>
                    </asp:BoundField>

                </Columns>
            </asp:GridView>

        </div>
    </div>
</asp:Content>

