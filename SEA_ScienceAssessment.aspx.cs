﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;


public partial class SEA_ScienceAssessment : System.Web.UI.Page
{
    string SchoolYear = "";
    string StateName = "";

    string CountCat;
    string sName;
    string sYear;
    // GridView gv = new GridView();
    protected void Page_Load(object sender, EventArgs e)
    {
        int StateID;
        if (!IsPostBack)
        {
            string currentDate = DateTime.Today.ToShortDateString();
            // displayRecords();
            loadData();
            DynamicGridView();
            lblheading.Text = "Score Student Count";
        }
        // CreateGridView();
        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void DynamicGridView()
    {
        ReportData rep1 = new ReportData();
        DataSet ds = new DataSet();
        ds = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_ScoreSCount");
        CreateGridView(ds);
    }

    //Display and load data
    private void displayRecords()
    {
        GetValues();
        lblheading.Text = "Score Student Count";
        ReportData rep1 = new ReportData();
        DataSet ds1 = new DataSet();
        // ds = rep1.Get_AllMath(SchoolYear, StateName, "spMath_SEAMathAssessment_EnrollmentSCount_SY11_14");
        ds1 = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_ScoreSCount");

        //  ds = rep1.Math(SchoolYear, "Math_EnrollmentSCount_SY11_14");
        CreateGridView(ds1);



    }
    private void loadData()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.SchoolYears
                      where p.SYearID > 7 && p.SYearID < 11
                      select p;
            chkSchoolyear.DataSource = qry;
            chkSchoolyear.DataTextField = "SchoolYear1";
            chkSchoolyear.DataValueField = "SYearID";
            chkSchoolyear.DataBind();

            var qryS = from e in db.States
                       where e.StateFullName != null && e.StateName != null
                       select e;
            ddlState.DataSource = qryS;
            ddlState.DataTextField = "StateFullName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();
            ddlState.Items.Insert(0, "");

            var qryM = from e in db.Math_CountTypes
                       where e.Math_CountID < 5
                       select e;
            ddlCount.DataSource = qryM;
            ddlCount.DataTextField = "Math_CountTypeName";
            ddlCount.DataValueField = "Math_CountID";
            ddlCount.DataBind();
            //ddlCount.Items.Insert(0, "Select");          


        }
    }

    //--Create Dynamic grid view--//
    private void CreateGridView(DataSet ds)
    {

        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        GridView gv = new GridView();
        gv.ID = "LEAGridView";
        gv.AutoGenerateColumns = false;
        gv.AllowPaging = false;
        gv.EnableViewState = false;
        // gv.PageSize = 10; // Default page Size
        // PlaceHolder1.Controls.Remove(gv);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    BoundField boundField = new BoundField();
                    boundField.DataField = dt.Columns[i].ColumnName.ToString();
                    boundField.HeaderText = dt.Columns[i].ColumnName.ToString();
                    gv.Columns.Add(boundField);
                }
            }
        }
        PlaceHolder1.Controls.Add(gv);
        BindGridView(gv, dt);
    }
    private void CreateGridView()
    {

        ReportData rep1 = new ReportData();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        ds = rep1.Get_AllSEAEnrolled_search(SchoolYear, StateName, "spSEA_Science_ScoreSCount");
        dt = ds.Tables[0];

        GridView gv = new GridView();
        gv.ID = "LEAGridView";
        gv.AutoGenerateColumns = false;
        gv.AllowPaging = false;
        gv.EnableViewState = false;
        // gv.PageSize = 10; // Default page Size
        // PlaceHolder1.Controls.Remove(gv);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    BoundField boundField = new BoundField();
                    boundField.DataField = dt.Columns[i].ColumnName.ToString();
                    boundField.HeaderText = dt.Columns[i].ColumnName.ToString();
                    gv.Columns.Add(boundField);
                }
            }
        }
        PlaceHolder1.Controls.Add(gv);
        BindGridView(gv, dt);

    }
    private void BindGridView(GridView gv, DataTable dt)
    {
        gv.DataSource = dt;
        gv.DataBind();

    }

    // clear and get search value
    private void ClearSearch()
    {
        // EnrData.Visible = true;
        divMessage.Text = "";
        chkSchoolyear.SelectedIndex = -1;
        ddlState.SelectedIndex = 0;
        ddlCount.SelectedIndex = 0;
    }
    private void GetValues()
    {
        // selected School Year

        if (chkSchoolyear.SelectedIndex != -1 && chkSchoolyear.SelectedItem.Text != "")
        {

            SchoolYear = chkSchoolyear.SelectedItem.Text;
        }
        else
        {
            SchoolYear = "";
        }


        // selected state name

        if (ddlState.SelectedIndex != -1 && ddlState.SelectedItem.Text != "")
        {
            StateName = ddlState.SelectedItem.Text;
        }
        else
        {
            StateName = "";
        }

        if (ddlCount.SelectedIndex != -1 && ddlCount.SelectedItem.Text != "")
        {
            CountCat = ddlCount.SelectedItem.Text;

            if (!(CountCat == null))
            {

                if (CountCat == "SEA Score Student Count")
                {
                    lblheading.Text = "Score Student Count";
                }
                else if (CountCat == "SEA Proficient Student Count")
                {
                    lblheading.Text = "Proficient Student Count";
                }
                else if (CountCat == "SEA Enrollment Student Count")
                {
                    lblheading.Text = "Enrollment Student Count";
                }
                else if (CountCat == "SEA Participated Student Count")
                {
                    lblheading.Text = "Participated Student Count";
                }
            }
            else
            {
                lblheading.Text = "";
            }

        }
        else
        {
            CountCat = "";
        }


    }


    //generate excel report 
    protected void lnkData_Click(object sender, EventArgs e)
    {
        GetValues();
        Get_ExcelData();
    }
    private void Get_ExcelData()
    {
        ReportData rep1 = new ReportData();
        if (CountCat != null && CountCat != "")
        {
            if (SchoolYear != "2014-2015")
            {
                if (CountCat == "SEA Score Student Count")
                {
                    DataSet ds = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_ScoreSCount");
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        ExportToExcel(ds, 0, Response, "ScoreStudentCount");
                    }
                }
                else if (CountCat == "SEA Proficient Student Count")
                {

                    DataSet ds = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_ProficientSCount");
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        ExportToExcel(ds, 0, Response, "ProficientStudentCount");
                    }
                }
                else if (CountCat == "SEA Enrollment Student Count")
                {

                    DataSet ds = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_EnrollmentSCount");
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        ExportToExcel(ds, 0, Response, "EnrollmentStudentCount");
                    }
                }
                else if (CountCat == "SEA Participated Student Count")
                {

                    DataSet ds = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_ParticipatedSCount");
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        ExportToExcel(ds, 0, Response, "ParticipatedStudentCount");
                    }
                }
            }
            else
            {
                // EnrData.Visible = false;
                divMessage.Text = "SEA Science Assessment data not found";
            }
        }
        else
        {
            DataSet ds = rep1.Get_English(SchoolYear, "spLEA_Science_ScoreSCount");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExportToExcel(ds, 0, Response, "ScoreStudentCount");
            }
        }
    }


    // excel format
    private void ExportToExcel(DataSet ds, int TableIndex, HttpResponse Response, string FileName)
    {
        int i, j;
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        // Response.ContentType = "application/vnd.ms-excel";
        string timestamp = Convert.ToString(DateTime.Now);
        timestamp = timestamp.Replace(" ", "_");
        timestamp = timestamp.Replace("/", "_");
        //  timestamp = timestamp.Replace(":", ":");
        FileName = FileName.Replace(" ", "");

        //string ExtractName = "Math" + "(" + timestamp + ")" + FileName + " .xlsx";
        string ExtractName = "SEA_Science_" + FileName + " .xlsx";
        //  Response.AppendHeader("content-disposition", "attachment; filename=" + ExtractName + ".xls");

        string sTempFileName = Server.MapPath(@"~\temp\dataeX_") + Guid.NewGuid().ToString() + ".xlsx";

        ExportDataSet(ds, sTempFileName);
        displayExport(sTempFileName, ExtractName);

    }
    public static void ExportDataSet(DataSet ds, string destination)
    {
        using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
        {
            var workbookPart = workbook.AddWorkbookPart();

            workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

            foreach (System.Data.DataTable table in ds.Tables)
            {

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                uint sheetId = 1;
                if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                {
                    sheetId =
                        sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }

                DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }


                sheetData.AppendChild(headerRow);

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (String col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        dynamic dc = dsrow[col];
                        if (dc.GetType().ToString() == "System.Boolean")
                        {
                            if (dsrow[col].ToString() == "True")
                            {
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Yes");
                            }

                        }
                        else
                        {
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString());
                        }
                        //if (Type.GetType(dc.GetType().ToString()))
                        //
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

            }
        }
    }
    private void displayExport(string sTempFileName, string sFilename)
    {
        try
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.spreadsheet";
            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);
            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);

            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);

            }
        }
    }
    // button to clear and search
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearSearch();
        displayRecords();

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        GetValues();
        Get_SearchData();
    }

    private void Get_SearchData()
    {
        ReportData rep1 = new ReportData();
        DataSet dsEnrolled = new DataSet();
        DataTable dt2 = new DataTable();
        if (CountCat != null && CountCat != "")
        {
            if (SchoolYear != "2014-2015")
            {
                if (CountCat == "SEA Score Student Count")
                {
                    // EnrData.Visible = true;
                    divMessage.Text = "";
                    dsEnrolled = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_ScoreSCount");
                    //   dsEnrolled = rep1.Math(SchoolYear, "Math_EnrollmentSCount_SY11_14");

                    if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
                    {
                        //dt2 = dsEnrolled.Tables[0];
                        //BindGridView(gv, dt2);
                        CreateGridView(dsEnrolled);
                    }
                }
                else if (CountCat == "SEA Proficient Student Count")
                {
                    //   EnrData.Visible = true;
                    divMessage.Text = "";
                    dsEnrolled = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_ProficientSCount");
                    if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
                    {
                        //dt2 = dsEnrolled.Tables[0];
                        //BindGridView(gv, dt2);
                        CreateGridView(dsEnrolled);
                    }
                }
                else if (CountCat == "SEA Enrollment Student Count")
                {
                    // EnrData.Visible = true;
                    divMessage.Text = "";
                    dsEnrolled = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_EnrollmentSCount");
                    if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
                    {
                        //dt2 = dsEnrolled.Tables[0];
                        //BindGridView(gv, dt2);
                        CreateGridView(dsEnrolled);
                    }
                }
                else if (CountCat == "SEA Participated Student Count")
                {
                    //EnrData.Visible = true;
                    divMessage.Text = "";
                    dsEnrolled = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spSEA_Science_ParticipatedSCount");
                    if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
                    {
                        //dt2 = dsEnrolled.Tables[0];
                        //BindGridView(gv, dt2);
                        CreateGridView(dsEnrolled);
                    }
                }
            }
            else
            {
                // EnrData.Visible = false;
                divMessage.Text = "SEA Science Assessment data not found";
            }
        }
    }
}