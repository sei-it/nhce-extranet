﻿<%@ Page Title="LEA Served" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LEA_Served.aspx.cs" Inherits="LEA_Served" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>LEA Served School Data</h2>
    <div>
        <h4>Select Year(s)</h4>
        <p style="align-items: center">
            <%-- <asp:CheckBoxList ID="chkSchoolyear" RepeatDirection="Horizontal" runat="server"></asp:CheckBoxList>--%>

            <asp:RadioButtonList ID="chkSchoolyear" runat="server" RepeatDirection="horizontal" Height="15px" Width="20px">
            </asp:RadioButtonList>

        </p>

        <h4>Select State</h4>
        <p>
            <asp:DropDownList ID="ddlState1" Visible="true" runat="server"></asp:DropDownList>
        </p>

        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
        <asp:Button ID="btnClear" runat="server" Text="Clear Search" OnClick="btnClear_Click" />
        <asp:Button ID="btnLEA" runat="server" Text="Get Individual LEA Data" OnClick="btnLEA_Click" />
    </div>
    <h2>LEA Student Count data</h2>
    <asp:Label ID="divMessage" runat="server" ForeColor="Red"></asp:Label>
    <div id="EnrData" runat="server">
        <p style="text-align: right; font-size: 18px">
            <asp:LinkButton ForeColor="Blue" ID="lnkData" Text="Download LEA Enrolled Data" runat="server" OnClick="lnkData_Click" />
        </p>

        <asp:PlaceHolder ID="PlaceHolder1" Visible="true" runat="server"></asp:PlaceHolder>
    </div>
</asp:Content>

