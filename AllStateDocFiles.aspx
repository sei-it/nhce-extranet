﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AllStateDocFiles.aspx.cs" Inherits="AllStateDocFiles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script lang="javascript">
        function printdiv(printpage) {
            var headstr = "<html><head><title></title></head><body>";
            var footstr = "</body>";
            // var newstr = document.all.item(printpage).innerHTML;
            var newstr = document.getElementById(printpage).innerHTML;
            var oldstr = document.body.innerHTML;
            document.body.innerHTML = headstr + newstr + footstr;
            window.print();
            document.body.innerHTML = oldstr;
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>STATE PAGES</h2>

    <div id="map" style="width: 100%"></div>
    <img class="legend" src="Images/legend.png" alt="Map Legend" />
    <div id="print">
        <asp:GridView ID="gvDocFiles" runat="server" Width="600px" CssClass="etacTbl" OnRowDataBound="gvDocFiles_RowDataBound" OnRowCommand="gvDocFiles_RowCommand"
            AutoGenerateColumns="false" AllowPaging="false" PageSize="15" OnPageIndexChanging="gvDocFiles_PageIndexChanging" AllowSorting="true"  OnSorting="gvDocFiles_Sorting">

            <Columns>

<%--
                 <asp:TemplateField>
                    <ItemTemplate>
                        <asp:CheckBox ID="chkRow" runat="server"  />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField>
                    <ItemTemplate>
                      <asp:ImageButton ID="imageCheck" runat="server" Width="8px" Height="8px" ImageUrl="Images/check.gif" Enabled="false" Visible="false" />
                    </ItemTemplate>
                </asp:TemplateField> 

                <asp:TemplateField HeaderText="State List" SortExpression="StateFullName" >
                    <ItemTemplate>
                        <a href="StateDocList.aspx?StateID=<%#Eval("StateID") %>"><b><%# Eval("StateFullName") %></b></a>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="State Action Plans" SortExpression="StateActionPlansDocID">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("StateActionPlansDocID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Data Workbooks" SortExpression="StateWorkbookDocID">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Style="align-content: center" Text='<%# Eval("StateWorkbookDocID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Premonitoring" SortExpression="PremonitoringDocID">
                    <ItemTemplate>

                        <%-- <asp:Label ID="Label3" runat="server" Text='<%# Eval("PremonitoringDocID")=="1" ? true: false %>'></asp:Label>--%>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("PremonitoringDocID") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="PremonitoringDetal" runat="server" CommandName="Detal" CommandArgument='<%# Eval("StateID") %>'>Premonitoring Sub Category</asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle Width="10px" />
                </asp:TemplateField>




            </Columns>
        </asp:GridView>

    </div>

    <p>
        <asp:Button ID="btnPrint" runat="server" class="rotate printme" Text="" OnClientClick="printdiv('print');" /></p>

    <asp:Literal ID="LitMessage" runat="server"></asp:Literal>

    <script src="scripts/mapdata.js"></script>
    <script src="scripts/usmap.js"></script>
</asp:Content>

