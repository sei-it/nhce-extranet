﻿<%@ Page Title="Premonitoring Document" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PremonitoringDocs.aspx.cs" Inherits="PremonitoringDocs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js"></script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript" src="Scripts/jquery.cookie.js"></script>


    <script>
        $(function () {

            var act = 0;
            $("#monDocs").accordion({

                //collapsible: false,
                //active: false ,

                create: function (event, ui) {
                    //get index in cookie on accordion create event
                    if ($.cookie('saved_index') != null) {
                        act = parseInt($.cookie('saved_index'));
                    }
                },
                change: function (event, ui) {
                    //set cookie for current index on change event
                    $.cookie('saved_index', null);
                    $.cookie('saved_index', ui.options.active);
                },

                active: parseInt($.cookie('saved_index'))
            });
        });
    </script>
    

    <h2>Premonitoring Documents</h2>
    <p>
        <span style="padding-left: 665px">

            <asp:LinkButton ID="lnkBack" Font-Bold="true" Text="Back" runat="server" OnClick="lnkBack_Click" /></span>
    </p>
    <p style="width: 700px">
        Before uploading your premonitoring documents, please check the box 
            or boxes next to the topic this document describes.  Once you have
             uploaded your document, it will be listed below under all of the 
            categories you selected.  You do not need to upload the document 
            more than once.
    </p>
    <div class="formLayout" style="padding-left: 35px">

        <p>

            <%-- <h3>Please upload a file for multiple pre-monitoring documents</h3>   --%>
            <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red"></asp:Label>

        <p>


            <label for="chkPm1"></label>
            <asp:CheckBox ID="chkPm1" runat="server" TextAlign="Right" Text="1.1 Monitoring and Evaluation of LEAs"></asp:CheckBox>
        </p>
        <p>
            <label for="chkPm2"></label>
            <asp:CheckBox ID="chkPm2" runat="server" Text="2.1 Identification, Enrollment, Retention through Coordination and Collaboration"></asp:CheckBox>
        </p>
        <p>
            <label for="chkPm3"></label>
            <asp:CheckBox ID="chkPm3" runat="server" Text="2.2 Technical Assistance to LEAs"></asp:CheckBox>
        </p>
        <p>
            <label for="chkPm4"></label>
            <asp:CheckBox ID="chkPm4" runat="server" Text="3.1 LEA Subgrants"></asp:CheckBox>
        </p>
        <p>
            <label for="chkPm5"></label>
            <asp:CheckBox ID="chkPm5" runat="server" Text="3.2 State-level Coordination Activities"></asp:CheckBox>
        </p>
        <p>
            <label for="chkPm6"></label>
            <asp:CheckBox ID="chkPm6" runat="server" Text="3.3 Dispute Resolution"></asp:CheckBox>
        </p>



        <p>
            <asp:FileUpload ID="FileUploadPre" Width="400px"  runat="server" />
            <asp:Button ID="ButbtnPremon" runat="server" Text="Upload" Visible="true" ValidationGroup="Submit" OnClick="ButbtnPremon_Click" />
        </p>
       
        <p>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                ControlToValidate="FileUploadPre" ErrorMessage="Only .jpg .pdf .docx .xlsx .gif .png .jpeg .doc .ppt files are allowed"
                ValidationExpression="(.*?)\.(jpg|pdf|docx|xlsx|gif|png|jpeg|doc|xls|PDF|PNG|PPT|ppt|pptx)$" ForeColor="Red" ValidationGroup="Submit">
            </asp:RegularExpressionValidator>
        </p>
       
    </div>
    <br />
    <br />



    <%--<div id="monDocs" style="width: 700px">--%>
<div  style="width: 700px">
        <h3>1.1 Monitoring and Evaluation of LEAs</h3>
        <div>
            <div>
                <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="600px"
                    DataKeyNames="PremonitoringDocID" CssClass="etacTbl" Style="margin-left: 8px;">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Uploaded file                    
                            </HeaderTemplate>
                            <ItemTemplate>
                              <a target="_blank"  href='<%# String.Format("PMonitoringEvaldownload.aspx?PhysicalFileName={0}&PremonitoringDocID={1}&Pre_MonitoringID_FK={2}",Eval("PhysicalFileName"),
                                 Eval("PremonitoringDocID"),Eval("Pre_MonitoringID_FK"))%>'>
                                    <%# Eval("OriginalFileName")%></a>
                            </ItemTemplate>

                            <HeaderStyle Width="550px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" class="cancelButton" Text="Delete" OnClientClick="return confirm('Are you certain you want to delete?');"
                                    OnClick="LinkButton1_Click" CommandArgument='<%# Eval("PremonitoringDocID")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            
            <div>
            </div>
        </div>

        <h3>2.1 Identification, Enrollment, Retention through Coordination and Collaboration</h3>
        <div>
            <div>
                <asp:GridView ID="GridView2" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="600px"
                    DataKeyNames="PremonitoringDocID" CssClass="etacTbl" Style="margin-left: 8px;">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Uploaded file                    
                            </HeaderTemplate>
                            <ItemTemplate>
                               <a target="_blank"  href='<%# String.Format("PMonitoringEvaldownload.aspx?PhysicalFileName={0}&PremonitoringDocID={1}&Pre_MonitoringID_FK={2}",Eval("PhysicalFileName"),
                                 Eval("PremonitoringDocID"),Eval("Pre_MonitoringID_FK"))%>'>
                                    <%# Eval("OriginalFileName")%></a>
                            </ItemTemplate>

                            <HeaderStyle Width="550px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" OnClientClick="return confirm('Are you certain you want to delete?');"
                                    OnClick="LinkButton2_Click" CommandArgument='<%# Eval("PremonitoringDocID")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
           


        </div>

        <h3>2.2 Technical Assistance to LEAs</h3>
        <div>
            <div>
                <asp:GridView ID="GridView3" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="600px"
                    DataKeyNames="PremonitoringDocID" CssClass="etacTbl" Style="margin-left: 8px;">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Uploaded file                    
                            </HeaderTemplate>
                            <ItemTemplate>
                              <a target="_blank"  href='<%# String.Format("PMonitoringEvaldownload.aspx?PhysicalFileName={0}&PremonitoringDocID={1}&Pre_MonitoringID_FK={2}",Eval("PhysicalFileName"),
                                 Eval("PremonitoringDocID"),Eval("Pre_MonitoringID_FK"))%>'>
                                    <%# Eval("OriginalFileName")%></a>
                            </ItemTemplate>

                            <HeaderStyle Width="550px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton3" runat="server" Text="Delete" OnClientClick="return confirm('Are you certain you want to delete?');"
                                    OnClick="LinkButton3_Click" CommandArgument='<%# Eval("PremonitoringDocID")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
         

        </div>

        <h3>3.1 LEA Subgrants</h3>
        <div>
            <div>
                <asp:GridView ID="GridView4" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="600px"
                    DataKeyNames="PremonitoringDocID" CssClass="etacTbl" Style="margin-left: 8px;">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Uploaded file                    
                            </HeaderTemplate>
                            <ItemTemplate>
                                <a target="_blank"  href='<%# String.Format("PMonitoringEvaldownload.aspx?PhysicalFileName={0}&PremonitoringDocID={1}&Pre_MonitoringID_FK={2}",Eval("PhysicalFileName"),
                                 Eval("PremonitoringDocID"),Eval("Pre_MonitoringID_FK"))%>'>
                                    <%# Eval("OriginalFileName")%></a>
                            </ItemTemplate>

                            <HeaderStyle Width="550px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" Text="Delete" OnClientClick="return confirm('Are you certain you want to delete?');"
                                    OnClick="LinkButton4_Click" CommandArgument='<%# Eval("PremonitoringDocID")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
           

        </div>

        <h3>3.2 State-level Coordination Activities</h3>
        <div>
            <div>
                <asp:GridView ID="GridView5" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="600px"
                    DataKeyNames="PremonitoringDocID" CssClass="etacTbl" Style="margin-left: 8px;">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Uploaded file                    
                            </HeaderTemplate>
                            <ItemTemplate>
                                <a target="_blank"  href='<%# String.Format("PMonitoringEvaldownload.aspx?PhysicalFileName={0}&PremonitoringDocID={1}&Pre_MonitoringID_FK={2}",Eval("PhysicalFileName"),
                                 Eval("PremonitoringDocID"),Eval("Pre_MonitoringID_FK"))%>'>
                                    <%# Eval("OriginalFileName")%></a>
                            </ItemTemplate>

                            <HeaderStyle Width="550px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton5" runat="server" Text="Delete" OnClientClick="return confirm('Are you certain you want to delete?');"
                                    OnClick="LinkButton5_Click" CommandArgument='<%# Eval("PremonitoringDocID")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
           

        </div>

        <h3>3.3 Dispute Resolution</h3>
        <div>
            <div>
                <asp:GridView ID="GridView6" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="600px"
                    DataKeyNames="PremonitoringDocID" CssClass="etacTbl" Style="margin-left: 8px;">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Uploaded file                    
                            </HeaderTemplate>
                            <ItemTemplate>
                                 <a target="_blank"  href='<%# String.Format("PMonitoringEvaldownload.aspx?PhysicalFileName={0}&PremonitoringDocID={1}&Pre_MonitoringID_FK={2}",Eval("PhysicalFileName"),
                                 Eval("PremonitoringDocID"),Eval("Pre_MonitoringID_FK"))%>'>
                                    <%# Eval("OriginalFileName")%></a>
                            </ItemTemplate>

                            <HeaderStyle Width="550px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton6" runat="server" Text="Delete" OnClientClick="return confirm('Are you certain you want to delete?');"
                                    OnClick="LinkButton6_Click" CommandArgument='<%# Eval("PremonitoringDocID")%>' />
                            </ItemTemplate>
                            <HeaderStyle Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
           

        </div>

    </div>
    <%--  <p><asp:Button ID="btUpload" runat="server" Text="Upload" Visible="false" ValidationGroup="Submit" OnClick="btUpload_Click"/></p>--%>
</asp:Content>

