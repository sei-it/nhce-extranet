﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;


/// <summary>
/// Summary description for ReportData
/// </summary>
public class ReportData     
{
    string connString = ConfigurationManager.ConnectionStrings["NCHEConnectionString"].ToString();
	public ReportData()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet Get_AllSEAEnrolled(string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;       
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllSEAServed(string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllSEAEnrolled_search(string SchoolYear, string StateName, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllSEAEnrolled(string StateName, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);        
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllSEAServed_search(string SchoolYear, string StateName, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllMath(string SchoolYear, string StateName, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllSEA_Math(string StateName,string SchoolYear, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds; 
    }

    public DataSet Get_AllSEA_Science(string SchoolYear, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds; 
    }

    public DataSet Get_AllSEA_English(string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds; 
    }

    public DataSet Math(string SchoolYear, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_English(string SchoolYear, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllPrefilled_Math(string StateName, string Year, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);
        cmd.Parameters.Add("@Year", Year);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }
}