var simplemaps_usmap_mapdata = {

	main_settings:{
		//General settings
		width: '700', //or 'responsive'
		background_color: '#FFFFFF',	
		background_transparent: 'no',
		border_color: '#ffffff',
		popups: 'on_click', //on_click, on_hover, or detect
	
		//State defaults
		state_description:   'State description',
		state_color: '#366DED',
		state_hover_color: '#000',
		state_url: 'http://simplemaps.com',
		border_size: 1.5,		
		all_states_inactive: 'no',
		all_states_zoomable: 'no',		
		
		//Location defaults
		location_description:  'Location description',
		location_color: '#FF0067',
		location_opacity: .8,
		location_hover_opacity: 1,
		location_url: '',
		location_size: 25,
		location_type: 'square', // circle, square, image
		location_image_source: 'frog.png', //name of image in the map_images folder		
		location_border_color: '#FFFFFF',
		location_border: 2,
		location_hover_border: 2.5,				
		all_locations_inactive: 'no',
		all_locations_hidden: 'no',
		
		//Labels
		label_color: '#d5ddec',	
		label_hover_color: '#d5ddec',		
		label_size: 22,
		label_font: 'Arial',
		hide_labels: 'no',
		hide_eastern_labels: 'no',
		
		//Zoom settings
		zoom: 'yes', //use default regions
		back_image: 'no',   //Use image instead of arrow for back zoom				
		arrow_color: '#3B729F',
		arrow_color_border: '#88A4BC',
		initial_back: 'no', //Show back button when zoomed out and do this JavaScript upon click		
		initial_zoom: -1,  //-1 is zoomed out, 0 is for the first continent etc	
		initial_zoom_solo: 'no', //hide adjacent states when starting map zoomed in
		region_opacity: 1,
		region_hover_opacity: .6,
		zoom_out_incrementally: 'yes',  // if no, map will zoom all the way out on click
		zoom_percentage: .99,
		zoom_time: .5, //time to zoom between regions in seconds
		
		//Popup settings
		popup_color: 'white',
		popup_opacity: .9,
		popup_shadow: 1,
		popup_corners: 5,
		popup_font: '12px/1.5 Verdana, Arial, Helvetica, sans-serif',
		popup_nocss: 'no', //use your own css	
		
		//Advanced settings
		div: 'map',
		auto_load: 'yes',		
		url_new_tab: 'no', 
		images_directory: 'default', //e.g. 'map_images/'
		fade_time:  .1, //time to fade out		
		link_text: 'Matrices'  //Text mobile browsers will see for links	
		
	},

	state_specific:{	
		"HI": {
			name: 'Hawaii',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=15">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=15">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=15">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=15'
		},
		"AK": {
			name: 'Alaska',
			//description: 'Project Downloads (2)<br />Manual Downloads (1)',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=1">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=1">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=1">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=1'
			},
		"FL": {
			name: 'Florida',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=11">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=11">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=11">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=11',
			inactive: 'no'
			},
		"NH": {
			name: 'New Hampshire',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=36">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=36">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=36">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=36'
			},
		"VT": {
			name: 'Vermont',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=54">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=54">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=54">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=54'
			},
		"ME": {
			name: 'Maine',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=25">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=25">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=25">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=25'
	
			},
		"RI": {
			name: 'Rhode Island',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=47">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=47">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=47">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=47'
			},
		"NY": {
			name: 'New York',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=40">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=40">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=40">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=40'	
		},
		"PA": {
			name: 'Pennsylvania',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=44">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=44">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=44">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=44'			
			},
		"NJ": {
			name: 'New Jersey',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=37">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=37">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=37">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=37'
			},
		"DE": {
			name: 'Delaware',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=10">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=10">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=10">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=10'
			},
		"MD": {
			name: 'Maryland',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=24">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=24">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=24">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=24'
						
			},
		"VA": {
			name: 'Virginia',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=53">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=53">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=53">Data Workbooks</a></p>',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=53'
			},
		"WV": {
			name: 'West Virginia',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=57">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=57">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=57">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=57'
			},
		"OH": {
			name: 'Ohio',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=41">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=41">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=41">Data Workbooks</a></p>',
			color: '#3A17B1',
			hover_color: 'default',
			url:'StateDocList.aspx?StateID=41'
			},
		"IN": {
			name: 'Indiana',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=19">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=19">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=19">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=19'
			},
		"IL": {
			name: 'Illinois',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=18">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=18">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=18">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=18'
			
			},
		"CT": {
			name: 'Connecticut',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=8">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=8">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=8">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=8'
			},
		"WI": {
			name: 'Wisconsin',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=58">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=58">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=58">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=58'
			},
		"NC": {
			name: 'North Carolina',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=33">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=33">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=33">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=33'
			},
		"DC": {
			name: 'District of Columbia',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=9">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=9">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=9">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=9'
		},
		"MA": {
			name: 'Massachusetts',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=23">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=23">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=23">Data Workbooks</a></p>',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=23'
				
			},
		"TN": {
			name: 'Tennessee',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=50">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=50">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=50">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=50'
			},
		"AR": {
			name: 'Arkansas',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=3">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=3">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=3">Data Workbooks</a></p>',

			color: '#3A17B1',
            //
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=3'
			},
		"MO": {
			name: 'Missouri',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=29">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=29">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=29">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=29'
			},
		"GA": {
			name: 'Georgia',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=13">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=13">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=13">Data Workbooks</a></p>',

			color: 'default',
            //
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=13'
			},
		"SC": {
			name: 'South Carolina',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=48">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=48">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=48">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=48'
			},
		"KY": {
			name: 'Kentucky',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=21">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=21">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=21">Data Workbooks</a></p>',

			color: '#3A17B1',
			zoomable: 'no',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=21'
			},
		"AL": {
			name: 'Alabama',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=2">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=2">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=2">Data Workbooks</a></p>',
			color: 'default',
            //
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=2'
					
			},
		"LA": {
			name: 'Louisiana',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=22">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=22">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=22">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=22'
			},
		"MS": {
			name: 'Mississippi',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=31">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=31">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=31">Data Workbooks</a></p>',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=31'
			},
		"IA": {
			name: 'Iowa',
		    //	description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=16">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=16">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=16">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=16'
			},
		"MN": {
			name: 'Minnesota',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=28">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=28">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=28">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=28'
			},
		"OK": {
			name: 'Oklahoma',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=42">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=42">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=42">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=42'
			},
		"TX": {
			name: 'Texas',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=51">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=51">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=51">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=51'
			},
		"NM": {
			name: 'New Mexico',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=38">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=38">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=38">Data Workbooks</a></p>',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=38'
			},
		"KS": {
			name: 'Kansas',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=20">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=20">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=20">Data Workbooks</a></p>',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=20'
			
			},
		"NE": {
			name: 'Nebraska',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=35">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=35">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=35">Data Workbooks</a></p>',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=35'
		
			},
		"SD": {
			name: 'South Dakota',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=49">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=49">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=49">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=49'
			},
		"ND": {
			name: 'North Dakota',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=34">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=34">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=34">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=34'
			},
		"WY": {
			name: 'Wyoming',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=59">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=59">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=59">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=59'
			},
		"MT": {
			name: 'Montana',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=32">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=32">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=32">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=32'
			},
		"CO": {
			name: 'Colorado',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=7">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=7">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=7">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=7'
			},
		"UT": {
			name: 'Utah',
		    //	description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=52">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=52">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=52">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=52'
			},
		"AZ": {
			name: 'Arizona',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=5">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=5">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=5">Data Workbooks</a></p>',

			color: 'default',
            //
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=5'
			},
		"NV": {
			name: 'Nevada',
		    //	description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=39">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=39">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=39">Data Workbooks</a></p>',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=39'
			},
		"OR": {
			name: 'Oregon',
		    //	description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=43">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=43">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=43">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=43'
			},
		"WA": {
			name: 'Washington',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=56">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=56">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=56">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=56'
			},
		"CA": {
			name: 'California',
		    //	description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=6">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=6">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=6">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=6'
			},
		"MI": {
			name: 'Michigan',
		    //description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=27">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=27">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=27">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=27'
			},
		"ID": {
			name: 'Idaho',
		    //	description: 'default',
			description: '<p><a class="pre" href="PremonitoringDocs.aspx?StateID=17">Premonitoring Documents</a></p><p><a class="action" href="StateActionPlans.aspx?StateID=17">State Action Plans</a></p><p><a  class="data" href="DataWorkbooks.aspx?StateID=17">Data Workbooks</a></p>',

			color: '#3A17B1',
			hover_color: 'default',
			url: 'StateDocList.aspx?StateID=17'
			},
		// Territories - Hidden unless hide is set to "no"
		"GU": {
			name: 'Guam',
			//description: 'default',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'default',
			hide: 'yes'
			},
		"VI": {
			name: 'Virgin Islands',
			image_source: 'x.png',			
			//description: 'default',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'default',
			hide: 'yes'
			},
		"PR": {
			name: 'Puerto Rico',
		//	description: 'default',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'default',
			hide: 'yes'
			},
		"MP": {
			name: 'Northern Mariana Islands',
			description: 'default',
			color: '#3A17B1',
			hover_color: 'default',
			url: 'default',
			hide: 'yes'
			}		
		},
	
	locations:{
		//"0": { //must give each location an id, so that you can reference it later
		//	name: "New York",
		//	lat: 40.71, 
		//	lng: -74.00,
		//	description: 'default',
		//	color: 'default',
		//	url: 'default',
		//	type: 'default',
		//	size: 'default' //Note:  You must omit the comma after the last property in an object to prevent errors in internet explorer.
		//},
		//1: {
		//	name: 'Anchorage',
		//	lat: 61.2180556,
		//	lng: -149.9002778, 
		//	color: 'default',
		//	type: 'circle'
		//}
	},
	
	labels:{
		"HI": {
		    color: '#3A17B1',
			hover_color: 'default',			
			font_family: 'default',
			pill: 'yes',	
			width: 'StateDocList.aspx?StateID=15',
		}
	}
	
}




