﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LEA_MathAssessment.aspx.cs" Inherits="LEA_MathAssessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>LEA Math Assessment Data</h2>
    <div>
        <h4>Select Year(s)</h4>
        <p style="align-items: center">
            <%-- <asp:CheckBoxList ID="chkSchoolyear" RepeatDirection="Horizontal" runat="server"></asp:CheckBoxList>--%>

            <asp:RadioButtonList ID="chkSchoolyear" runat="server" RepeatDirection="horizontal" Height="15px" Width="20px">
            </asp:RadioButtonList>

        </p>

        <h4>Select State</h4>
        <p>
            <asp:DropDownList ID="ddlState" Visible="true" runat="server"></asp:DropDownList>
        </p>
        <h4>Select Student Count Type</h4>
        <p>
        <p>
            <asp:DropDownList ID="ddlCount" Visible="true" runat="server"></asp:DropDownList>
        </p>


        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
        <asp:Button ID="btnClear" runat="server" Text="Clear Search" OnClick="btnClear_Click" />
        <asp:Button ID="btnLEA" runat="server" Text="Get Individual LEA Data" OnClick="btnLEA_Click" />
    </div>
    <h2>LEA Math Assessment
        <asp:Label ID="lblheading" runat="server"></asp:Label></h2>
    <asp:Label ID="divMessage" runat="server" ForeColor="Red"></asp:Label>
    <div >
        <p style="text-align: right; font-size: 18px">
            <asp:LinkButton ForeColor="Blue" ID="lnkData" Text="Download LEA Enrolled Data" runat="server" OnClick="lnkData_Click" />
        </p>
         <asp:PlaceHolder ID="PlaceHolder1" Visible="true" runat="server"></asp:PlaceHolder>
    </div>
</asp:Content>

