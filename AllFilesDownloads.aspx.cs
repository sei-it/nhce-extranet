﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Configuration;


public partial class AllFilesDownloads : System.Web.UI.Page
{
    int PMonitoringEval = 1;
    int PStateLocalAgencyPR = 2;
    int PFiscalOversight = 3;
    int PMcKinneyMonitoringEval = 4;
    int PMcKinneyVentoProgS = 5;
    int PMcKinneyVentoFiscalO = 6;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    AppUser oAU = (from ans in db.AppUsers
                                   where ans.sUserName == Context.User.Identity.Name.ToString()
                                   select ans).FirstOrDefault();

                    if ((oAU != null))
                    {
                        if (!(oAU.RoleID_FK == 1 || oAU.RoleID_FK == 2))                       
                        {
                            Response.Redirect("AllFilesdownloadError.aspx");
                        }
                    }
                }

            }
        }
    }

    
    protected void btnStateWorkbooks_Click(object sender, EventArgs e)
    {
        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.StateDocs                     
                      select ans;

           // string[] filePaths = Directory.GetFiles(Server.MapPath("~/Uploads/"));
            string sourceFile = ConfigurationManager.AppSettings["AppFilePath"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);             
              
          
            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.FileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));                        
                    }
                }
            }           
        }

       // string zipFileName = "StateWorkbooks.zip";
        string zipFileName = String.Format("StateWorkbooks_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownloadStatebooks(zipFileName, files);        
    }

    private void ZipFilesDownloadStatebooks(string zipFileName, List<ListItem> files)
    {
        //// Here we will create zip file & download    
        Response.ContentType = "application/zip";
        Response.AddHeader("content-disposition", "fileName=" + zipFileName);

        using (var zipStream = new ZipOutputStream(Response.OutputStream))
        {

            foreach (ListItem item in files)
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(item.Value);

                var fileEntry = new ZipEntry(Path.GetFileName(item.Value))
                {
                    Size = fileBytes.Length
                };

                zipStream.PutNextEntry(fileEntry);
                zipStream.Write(fileBytes, 0, fileBytes.Length);
            }

            zipStream.Flush();
            zipStream.Close();
        }

    }

    
    protected void Pre1_Click(object sender, EventArgs e)
    {
        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.PremonitoringUploads
                      where ans.SubPremonitoring == PMonitoringEval
                      select ans;

          //  string[] filePaths = Directory.GetFiles(Server.MapPath("~/PMonitoringEval/"));
            string sourceFile = ConfigurationManager.AppSettings["Premonitoring1.1"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);
           

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));     
                    }
                }
            }
        }

        //string zipFileName = "MonitoringEval.zip";
        string zipFileName = String.Format("1.1_MonitoringEval_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(zipFileName, files);     
    }
    protected void Pre2_Click(object sender, EventArgs e)
    {

        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.PremonitoringUploads
                      where ans.SubPremonitoring == PStateLocalAgencyPR
                      select ans;

          //  string[] filePaths = Directory.GetFiles(Server.MapPath("~/PStateLocalAgencyPR/"));
            string sourceFile = ConfigurationManager.AppSettings["Premonitoring2.1"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);
            

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                    }
                }
            }
        }
        // string zipFileName = "StateLocalAgencyPR.zip";
        string zipFileName = String.Format("2.1_StateLocalAgencyPR_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(zipFileName, files);
    }   
    protected void Pre3_Click(object sender, EventArgs e)
    {

        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.PremonitoringUploads
                      where ans.SubPremonitoring == PFiscalOversight
                      select ans;

            //string[] filePaths = Directory.GetFiles(Server.MapPath("~/PFiscalOversight/"));
            string sourceFile = ConfigurationManager.AppSettings["Premonitoring2.2"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);
           

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                    }
                }
            }
        }
       // string zipFileName = "FiscalOversight.zip";
        string zipFileName = String.Format("2.2_FiscalOversight_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(zipFileName, files);
    }
    protected void Pre4_Click(object sender, EventArgs e)
    {
        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.PremonitoringUploads
                      where ans.SubPremonitoring == PMcKinneyMonitoringEval
                      select ans;

           // string[] filePaths = Directory.GetFiles(Server.MapPath("~/PMcKinneyMonitoringEval/"));
            string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.1"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);
            

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                    }
                }
            }
        }
       // string zipFileName = "McKinneyMonitoringEval.zip";
        string zipFileName = String.Format("3.1_McKinneyMonitoringEval_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(zipFileName, files);
    }
    protected void Pre5_Click(object sender, EventArgs e)
    {

        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.PremonitoringUploads
                      where ans.SubPremonitoring == PMcKinneyVentoProgS
                      select ans;

           // string[] filePaths = Directory.GetFiles(Server.MapPath("~/PMcKinneyVentoProgS/"));
            string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.2"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);
           

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                    }
                }
            }
        }
       // string zipFileName = "McKinneyVentoProgSupport.zip";
        string zipFileName = String.Format("3.2_McKinneyVentoProgSupport_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(zipFileName, files);
    }
    protected void Pre6_Click(object sender, EventArgs e)
    {
        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.PremonitoringUploads
                      where ans.SubPremonitoring == PMcKinneyVentoFiscalO
                      select ans;

           // string[] filePaths = Directory.GetFiles(Server.MapPath("~/PMcKinneyVentoFiscalO/"));
            string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.3"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);
            

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                    }
                }
            }
        }
        // string zipFileName = "McKinneyVentoFiscalOversight.zip";
        string zipFileName = String.Format("3.3_McKinneyVentoFiscalOversight_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(zipFileName, files);
    }
    protected void DataWork_Click(object sender, EventArgs e)
    {
        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.StateWorkbookUploads                      
                      select ans;

            //string[] filePaths = Directory.GetFiles(Server.MapPath("~/DataWorkbooksDocs/"));
            string sourceFile = ConfigurationManager.AppSettings["DataWorkbooks"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);
           

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                    }
                }
            }
        }
       // string zipFileName = "DataWorkbooksDocs.zip";
        string zipFileName = String.Format("DataWorkbooksDocs_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(zipFileName, files);
    }
    protected void StateAction_Click(object sender, EventArgs e)
    {

        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.StateActionPlansUploads
                      select ans;

            //string[] filePaths = Directory.GetFiles(Server.MapPath("~/StateActionDocs/"));
            string sourceFile = ConfigurationManager.AppSettings["StateAction"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                    }
                }
            }
        }
       // string zipFileName = "StateActionPlansDocs.zip";
        string zipFileName = String.Format("StateActionPlansDocs_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(zipFileName, files);
    }
    private void ZipFilesDownload(string zipFileName, List<ListItem> files)
    {
        //// Here we will create zip file & download    
        Response.ContentType = "application/zip";
        Response.AddHeader("content-disposition", "fileName=" + zipFileName);

        using (var zipStream = new ZipOutputStream(Response.OutputStream))
        {

            foreach (ListItem item in files)
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(item.Value);

                var fileEntry = new ZipEntry(Path.GetFileName(item.Value).Substring(37))
                {
                    Size = fileBytes.Length
                };

                zipStream.PutNextEntry(fileEntry);
                zipStream.Write(fileBytes, 0, fileBytes.Length);
            }

            zipStream.Flush();
            zipStream.Close();
        }

    }
   
}