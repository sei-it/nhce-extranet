﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;


public partial class PreFilledReport : System.Web.UI.Page
{
    string Year = "";
    string StateName = "";
    string CountCat;
    string sName;
    string sYear;
    int reportID;
    string  sreport;
    int report;
    protected void Page_Load(object sender, EventArgs e)
    {
        int StateID;
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["reportID"] != null)
            {
                reportID = Convert.ToInt32(Page.Request.QueryString["reportID"].ToString());
                switch (reportID)
                {
                    case 1:
                        lblpreReportHeading.Text = "Report 1:  Comparison of Homeless Children and Youth Enrolled in Pre-Kindergarten through Grade 13 and Proportion of People Under the Age of 18 Living Below the Poverty Line";
                        preReport.Text = "This report compares the number of homeless children and youth enrolled in pre-Kindergarten (ages 3 to 5, not Kindergarten) through grade 13 with the proportion of people under the age of 18 living below the poverty line in that state.  Each of the raw numbers is presented followed by the quartile for that number.  Those with a 1 are in the first quartile – this is the lowest 25% of the scores given.  Those with a 4 are in the fourth quartile and are the highest 25% of the scores given.  Those states with a 2 or 3 are in the second or third quartile (respectively) and are in the middle of the values given for that variable.  The data on poverty are from the U.S. Census Bureau, 2010-2014 American Community Survey 5-Year Estimate. ";
                        lblheading.Text = "Total Homeless Student Enrollment";
                        lnkData.Visible = false;
                        report = 1;
                        break;
                    case 2:
                        lblpreReportHeading.Text = "Report 2:  Homeless Graduation Rates";
                        preReport.Text = "This report displays the Homeless Graduation Rates Data";
                        lblheading.Text = "Homeless Graduation Rates";
                        lnkData.Visible = false;
                        report = 2;
                        break;
                    case 3:
                        lblpreReportHeading.Text = "Report 3: Proportion of Homeless Children and Youth Assigned a Valid Score on Mathematics Assessments who were Proficient";
                        preReport.Text = "This report displays the proportion of homeless children and youth who received a valid score on a mathematics assessment who were at or above proficiency in mathematics.  Each of the raw numbers is presented followed by the quartile for that number.  Those with a 1 are in the first quartile – this is the lowest 25% of the scores given.  Those with a 4 are in the fourth quartile and are the highest 25% of the scores given.  Those states with a 2 or 3 are in the second or third quartile (respectively) and are in the middle of the values given for that variable. ";
                        lblheading.Text = "Number of HCY Proficient in Mathematics";
                        report = 3;
                        displayMath();
                        break;

                    case 4:
                        lblpreReportHeading.Text = "Report 4:  Proportion of Homeless Children and Youth Assigned a Valid Score on English/Language Arts Assessments who were Proficient";
                        preReport.Text = "This report displays the proportion of homeless children and youth who received a valid score on a English/Language Arts assessment who were at or above proficiency in English/Language Arts.  Each of the raw numbers is presented followed by the quartile for that number.  Those with a 1 are in the first quartile – this is the lowest 25% of the scores given.  Those with a 4 are in the fourth quartile and are the highest 25% of the scores given.  Those states with a 2 or 3 are in the second or third quartile (respectively) and are in the middle of the values given for that ";
                        lblheading.Text = "Number of HCY Proficient in ELA";
                        report = 4;
                        displayEla();
                        break;
                  

                }

            }
            string currentDate = DateTime.Today.ToShortDateString();
            // displayRecords();
            loadData();           
           
        }
        
        Page.MaintainScrollPositionOnPostBack = true;
    }

    // default grid data
    private void displayMath()
    {
       
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
           
            var vUsers = from p in db.vw_math_PerOverall_Quartile_Alls
                         orderby p.StateName
                         select p;
            grdVwList.DataSource = vUsers;
            grdVwList.DataBind();
        }

   
    }
    private void displayEla()
    {

        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var vUsers = from p in db.vw_ela_PerOverall_Quartile_Alls
                         orderby p.StateName
                         select p;
            grdVwList.DataSource = vUsers;
            grdVwList.DataBind();
        }


    }
   
    //Display and load data   
    private void loadData()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry = from p in db.SchoolYears
                      where p.SYearID > 7 && p.SYearID < 11
                      select p;
            chkSchoolyear.DataSource = qry;
            chkSchoolyear.DataTextField = "SchoolYear1";
            chkSchoolyear.DataValueField = "SYearID";
            chkSchoolyear.DataBind();

            var qryS = from e in db.States
                       where e.StateFullName != null && e.StateName != null
                       select e;
            ddlState.DataSource = qryS;
            ddlState.DataTextField = "StateFullName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();
            ddlState.Items.Insert(0, "");

                

        }
       
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["report"] != null)))
        {
            sreport = this.ViewState["report"].ToString();
            report = Convert.ToInt32(sreport);
        }       

    }
    protected override object SaveViewState()
    {
        this.ViewState["report"] = report;
        return (base.SaveViewState());
    }
    
    //clear and get search value
    private void ClearSearch()
    {
        // EnrData.Visible = true;
        divMessage.Text = "";
        chkSchoolyear.SelectedIndex = -1;
        ddlState.SelectedIndex = 0;
       
    }
    private void GetValues()
    {
        // selected School Year

        if (chkSchoolyear.SelectedIndex != -1 && chkSchoolyear.SelectedItem.Text != "")
        {

            Year = chkSchoolyear.SelectedItem.Text;
        }
        else
        {
            Year = "";
        }


        // selected state name

        if (ddlState.SelectedIndex != -1 && ddlState.SelectedItem.Text != "")
        {
            StateName = ddlState.SelectedItem.Text;
        }
        else
        {
            StateName = "";
        }       
    }

    //generate excel report 
    protected void lnkData_Click(object sender, EventArgs e)
    {
        GetValues();
        if (report == 3)
        {
            Get_MathExcelData();
        }
        else if (report == 4)
        {
            Get_ELAExcelData();
        }
        
    }
   
    private void Get_MathExcelData()
    {       
        ReportData rep1 = new ReportData();
        DataSet dsEnrolled = new DataSet();
        // EnrData.Visible = true;
        divMessage.Text = "";
        dsEnrolled = rep1.Get_AllPrefilled_Math(StateName, Year, "spMath_PrefilledReport");       

        if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
        {
            MathExportToExcel(dsEnrolled, 0, Response, "PreFilledReport");
        }       
    }
    private void MathExportToExcel(DataSet ds, int TableIndex, HttpResponse Response, string FileName)
    {
        int i, j;
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        // Response.ContentType = "application/vnd.ms-excel";
        string timestamp = Convert.ToString(DateTime.Now);
        timestamp = timestamp.Replace(" ", "_");
        timestamp = timestamp.Replace("/", "_");
        //  timestamp = timestamp.Replace(":", ":");
        FileName = FileName.Replace(" ", "");

        //string ExtractName = "Math" + "(" + timestamp + ")" + FileName + " .xlsx";
        string ExtractName = "Math_" + FileName + " .xlsx";
        //  Response.AppendHeader("content-disposition", "attachment; filename=" + ExtractName + ".xls");

        string sTempFileName = Server.MapPath(@"~\temp\dataeX_") + Guid.NewGuid().ToString() + ".xlsx";

        ExportDataSet(ds, sTempFileName);
        displayExport(sTempFileName, ExtractName);

    }

    private void Get_ELAExcelData()
    {
        ReportData rep1 = new ReportData();
        DataSet dsEnrolled = new DataSet();


        // EnrData.Visible = true;
        divMessage.Text = "";
        dsEnrolled = rep1.Get_AllPrefilled_Math(StateName, Year, "spEla_PrefilledReport");


        if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
        {
            ELAExportToExcel(dsEnrolled, 0, Response, "PreFilledReport");
        }

    }
    private void ELAExportToExcel(DataSet ds, int TableIndex, HttpResponse Response, string FileName)
    {
        int i, j;
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        // Response.ContentType = "application/vnd.ms-excel";
        string timestamp = Convert.ToString(DateTime.Now);
        timestamp = timestamp.Replace(" ", "_");
        timestamp = timestamp.Replace("/", "_");
        //  timestamp = timestamp.Replace(":", ":");
        FileName = FileName.Replace(" ", "");

        //string ExtractName = "Math" + "(" + timestamp + ")" + FileName + " .xlsx";
        string ExtractName = "ELA_" + FileName + " .xlsx";
        //  Response.AppendHeader("content-disposition", "attachment; filename=" + ExtractName + ".xls");

        string sTempFileName = Server.MapPath(@"~\temp\dataeX_") + Guid.NewGuid().ToString() + ".xlsx";

        ExportDataSet(ds, sTempFileName);
        displayExport(sTempFileName, ExtractName);

    }

    // excel format   
    public static void ExportDataSet(DataSet ds, string destination)
    {
        using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
        {
            var workbookPart = workbook.AddWorkbookPart();

            workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

            foreach (System.Data.DataTable table in ds.Tables)
            {

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                uint sheetId = 1;
                if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                {
                    sheetId =
                        sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }

                DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }


                sheetData.AppendChild(headerRow);

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (String col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        dynamic dc = dsrow[col];
                        if (dc.GetType().ToString() == "System.Boolean")
                        {
                            if (dsrow[col].ToString() == "True")
                            {
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Yes");
                            }

                        }
                        else
                        {
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString());
                        }
                        //if (Type.GetType(dc.GetType().ToString()))
                        //
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

            }
        }
    }
    private void displayExport(string sTempFileName, string sFilename)
    {
        try
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.spreadsheet";
            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);
            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);

            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);

            }
        }
    }

    // button to clear and search
    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearSearch();
        if (report == 3)
        {
            displayMath();
        }
        else if (report == 4)
        {
            displayEla();
        }       

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        GetValues();

        if (report == 3)
        {
            Get_MathSearchData();
        }
        else if (report == 4)
        {
            Get_ELASearchData();
        }
    }

    //get search data in grid
    private void Get_MathSearchData()
    {
        ReportData rep1 = new ReportData();
        DataSet dsEnrolled = new DataSet();
       
       
            
                    // EnrData.Visible = true;
                    divMessage.Text = "";
                    dsEnrolled = rep1.Get_AllPrefilled_Math(StateName, Year, "spMath_PrefilledReport");
                    //   dsEnrolled = rep1.Math(SchoolYear, "Math_EnrollmentSCount_SY11_14");

                    if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
                    {
                        grdVwList.DataSource = dsEnrolled;
                        grdVwList.DataBind();                         
                    }
               
    }
    private void Get_ELASearchData()
    {
        ReportData rep1 = new ReportData();
        DataSet dsEnrolled = new DataSet();

        // EnrData.Visible = true;
        divMessage.Text = "";
        dsEnrolled = rep1.Get_AllPrefilled_Math(StateName, Year, "spEla_PrefilledReport");
        //   dsEnrolled = rep1.Math(SchoolYear, "Math_EnrollmentSCount_SY11_14");

        if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
        {
            grdVwList.DataSource = dsEnrolled;
            grdVwList.DataBind();
        }
               
    }

    //Get All Grades data
    protected void btnGetGrades_Click(object sender, EventArgs e)
    {
        GetValues();
        if (report == 3)
        {
            Response.Redirect("Math_PreFilledReport.aspx?StateName=" + StateName + "&Year=" + Year);
        }
        else if (report == 4)
        {
            Response.Redirect("ELA_PrefilledReport.aspx?StateName=" + StateName + "&Year=" + Year);
        }
       
    }
}