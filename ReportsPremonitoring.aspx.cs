﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class ReportsPremonitoring : System.Web.UI.Page
{
    string sqlCon = ConfigurationManager.ConnectionStrings["NCHEConnectionString"].ConnectionString;
    int StateID;
    int Roleid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["StateID_fk"] != null)
            {
                StateID = Convert.ToInt32(Page.Request.QueryString["StateID_fk"]);
            }
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    AppUser oAU = (from ans in db.AppUsers
                                   where ans.sUserName == Context.User.Identity.Name.ToString()
                                   select ans).FirstOrDefault();

                    if ((oAU != null))
                    {
                        if (oAU.RoleID_FK == 1 || oAU.RoleID_FK == 2)
                        {
                            displayStatus();
                        }
                        else
                        {
                            Response.Redirect("AllFilesdownloadError.aspx");
                        }
                    }
                }

            }
        }
    }
    protected void displayStatus()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            BindData();

        }
    }

    private void BindData()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
           
            var oQnTOC = from m in db.vw_PremonitoringSubSections
                         where m.StateFullName != null & m.StateID == StateID
                         orderby m.StateFullName
                         select m;
            gvDocFiles.DataSource = oQnTOC;
            gvDocFiles.DataBind();
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["StateID_fk"] != null)))
        {
            StateID = Convert.ToInt32(this.ViewState["StateID_fk"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["StateID_fk"] = StateID;
        return (base.SaveViewState());
    }
    protected void gvDocFiles_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label myLabel1 = (Label)e.Row.FindControl("Label1");
            Label myLabel2 = (Label)e.Row.FindControl("Label2");
            Label myLabel3 = (Label)e.Row.FindControl("Label3");
            Label myLabel4 = (Label)e.Row.FindControl("Label4");
            Label myLabel5 = (Label)e.Row.FindControl("Label5");
            Label myLabel6 = (Label)e.Row.FindControl("Label6");
            ImageButton myimagebutton = (ImageButton)e.Row.FindControl("imageCheck");
            string text, text2, text3, text4, text5, text6;

            text = myLabel1.Text;

            text2 = myLabel2.Text;

            text3 = myLabel3.Text;

            text4 = myLabel4.Text;
            text5 = myLabel5.Text;
            text6 = myLabel6.Text;


            if (text == null || text.Length < 1)
            {
                myLabel1.Text = "Pending";
            }
            else
            {
                myLabel1.Text = "Uploaded";
            }

            if (text2 == null || text2.Length < 1)
            {
                myLabel2.Text = "Pending";
            }
            else
            {
                myLabel2.Text = "Uploaded";
            }

            if (text3 == null || text3.Length < 1)
            {
                myLabel3.Text = "Pending";
            }
            else
            {
                myLabel3.Text = "Uploaded";
            }

            if (text4 == null || text4.Length < 1)
            {
                myLabel4.Text = "Pending";
            }
            else
            {
                myLabel4.Text = "Uploaded";
            }

            if (text5 == null || text5.Length < 1)
            {
                myLabel5.Text = "Pending";
            }
            else
            {
                myLabel5.Text = "Uploaded";
            }

            if (text6 == null || text6.Length < 1)
            {
                myLabel6.Text = "Pending";
            }
            else
            {
                myLabel6.Text = "Uploaded";
            }

            if ((text.Length == 1) && (text2.Length == 1) && (text3.Length == 1) && (text4.Length == 1) && (text5.Length == 1) && (text6.Length == 1))
            {
                //  mycheckbox.Checked = true;
                myimagebutton.Visible = true;
            }
            else
            {
                //    mycheckbox.Checked = false;
                myimagebutton.Visible = false;
            }

            
        }
    }
}