﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class fileupload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnUpload_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            foreach (RepeaterItem item in rptUpload.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    FileUpload fu = (FileUpload)item.FindControl("fu");
                    if (fu.HasFile)
                    {
                        string path = Server.MapPath("~/StateActionDocs/");
                        string fileName = Path.GetFileName(fu.FileName);
                        string fileExt = Path.GetExtension(fu.FileName).ToLower();
                        fu.SaveAs(path + fileName + fileExt);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}