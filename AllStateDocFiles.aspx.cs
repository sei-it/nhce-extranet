﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class AllStateDocFiles : System.Web.UI.Page
{
    string sqlCon = ConfigurationManager.ConnectionStrings["NCHEConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    displayStatus();
        //}

        if (!IsPostBack)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    AppUser oAU = (from ans in db.AppUsers
                                   where ans.sUserName == Context.User.Identity.Name.ToString()
                                   select ans).FirstOrDefault();

                    if ((oAU != null))
                    {
                        if (oAU.RoleID_FK == 1 || oAU.RoleID_FK == 2)
                        {
                            displayStatus();
                        }
                        else
                        {
                            Response.Redirect("AllFilesdownloadError.aspx");
                        }
                    }
                }

            }
        }
        Page.MaintainScrollPositionOnPostBack=true;
    }

   
    protected void displayStatus()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            BindData();    
          
        }
    }

    private void BindData()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {         
            //var oQnTOC = (from m in db.States
            //              where m.StateFullName != null
            //              orderby m.StateFullName
            //              select m).ToList();

            var oQnTOC = from m in db.View_2s
                         where m.StateFullName != null
                         orderby m.StateFullName
                         select m;
            gvDocFiles.DataSource = oQnTOC;
            gvDocFiles.DataBind();
           
        }
    }
    protected void gvDocFiles_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocFiles.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void gvDocFiles_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label myLabel1 = (Label)e.Row.FindControl("Label1");
            Label myLabel2 = (Label)e.Row.FindControl("Label2");
            Label myLabel3 = (Label)e.Row.FindControl("Label3");
           // CheckBox mycheckbox = (CheckBox)e.Row.FindControl("chkRow");
            ImageButton myimagebutton = (ImageButton)e.Row.FindControl("imageCheck");

            string text;
            text = myLabel1.Text;

            string text2 = myLabel2.Text;

            string text3 = myLabel3.Text;


            if (text == null || text.Length < 1)
            {
                myLabel1.Text = "Pending";               
            }
            else
            {
                myLabel1.Text = "Uploaded";            
            }

            if (text2 == null || text2.Length < 1)
            {
                myLabel2.Text = "Pending";
            }
            else
            {
                myLabel2.Text = "Uploaded";
            }

            if (text3 == null || text3.Length < 1)
            {
                myLabel3.Text = "Pending";
            }
            else
            {
                myLabel3.Text = "Uploaded";
            }

            if ((text.Length == 1) && (text2.Length == 1) && (text3.Length == 1))
            {
             //   mycheckbox.Checked = true;
                myimagebutton.Visible = true;
            }
            else
            {
            //    mycheckbox.Checked = false;
                myimagebutton.Visible = false;
            }

        }
    }
    protected void gvDocFiles_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Detal")
        {
            int intStateID = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("ReportsPremonitoring.aspx?StateID_fk=" + intStateID);
        }
    }
    protected void gvDocFiles_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortingDirection = string.Empty;
        if (direction == SortDirection.Ascending)
        {
            direction = SortDirection.Descending;
            sortingDirection = "Desc";
        }
        else
        {
            direction = SortDirection.Ascending;
            sortingDirection = "Asc";
        }

        DataView sortedView = new DataView();
        sortedView = BindGridView();
        sortedView.Sort = e.SortExpression + " " + sortingDirection;
        Session["SortedView"] = sortedView;
        gvDocFiles.DataSource = sortedView;
        gvDocFiles.DataBind();
    }
    public SortDirection direction
    {
        get
        {
            if (ViewState["directionState"] == null)
            {
                ViewState["directionState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["directionState"];
        }
        set
        {
            ViewState["directionState"] = value;
        }
    }

    private DataView BindGridView()
    {
      
        DataView dv = new DataView();
        string query = "SELECT * FROM View_2 where StateFullName !=''  order by StateFullName";
        DataSet ds = new DataSet();
        SqlDataAdapter DA = new SqlDataAdapter(query, sqlCon);
        DA.Fill(ds);
        dv = new DataView(ds.Tables[0]);
        return dv;
    }
}
