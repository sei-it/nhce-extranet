﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="StateDocList.aspx.cs" Inherits="StateDocList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
        <h1><asp:Label Id="lbStateName" runat="server"></asp:Label> State Matrices</h1>
        <div class="matrices"><asp:GridView ID="gvDocFiles" runat="server"   class="martices" CssClass="etacTbl"  AutoGenerateColumns="false"  Width="600"  >
            <Columns>
                <asp:BoundField DataField="FileName" DataFormatString="{0:g}" HeaderText="File Name">
                    <HeaderStyle Width="550px" />
                </asp:BoundField>
                  <asp:TemplateField>  
                    <ItemTemplate>
                          <%--<a href="StateDocFiles.aspx?FileID=<%#Eval("StateDocID") %>" target="_blank" >Download</a>--%>
                        <a href="StateDocFiles.aspx?FileID=<%#Eval("StateDocID") %>" target="_blank" >Download</a>
                     </ItemTemplate>
                    <ItemStyle  Width="60px"></ItemStyle>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        </div>
   
    <div></div>
<div class="columns3"> 
    <div class="colx col1"> 
     <asp:LinkButton  id="LinkButton2"  runat="server" OnClick="LinkButton2_Click"><img class="premonitoring hvr-pop" src="Images/document_icon.png" alt="Premonitoring Documents">
   <h2>Premonitoring Documents</h2></asp:LinkButton>
      <p>Click here to upload documents for the federal program officer to use in the monitoring process.  </p>
   <asp:LinkButton ForeColor="Blue" id="lnkPremonitiring" Text="Upload Premonitoring Documents" runat="server" OnClick="lnkPremonitiring_Click"/>
	</div> 
	<div class="colx col2">
   <asp:LinkButton  id="LinkButton3"  runat="server" OnClick="LinkButton3_Click"> <img class="premonitoring hvr-pop" src="Images/workbooks_icon.png" alt="Premonitoring Documents">
    <h2>Data Workbooks</h2></asp:LinkButton>
    <p>Click here for data from the Consolidated State Progress Report (CSPR) compiled in an easy-to-use spreadsheet, including aggregated state-wide and LEA-specific results.</p>
     <asp:LinkButton ForeColor="Blue" id="lnkWorkbook" Text="Upload Data Workbooks" runat="server" OnClick="lnkWorkbook_Click"/>
     </div>
 
     <div class="colx col3">
    <asp:LinkButton  id="LinkButton4"  runat="server" OnClick="LinkButton4_Click"> <img class="premonitoring hvr-pop" src="Images/action_icon.png" alt="Premonitoring Documents">
     <h2>State Action Plans</h2></asp:LinkButton>
   <p>Upload your original state plan, revised state plan, and/or an annual action plan here.</p>
    <asp:LinkButton ForeColor="Blue" id="lnkStateAction" Text="Upload State Action Plans" runat="server" OnClick="lnkStateAction_Click"/>
    </div>

    </div>
    
    <asp:Literal ID="LitMessage" runat="server"></asp:Literal>
</asp:Content>

