﻿<%@ Page Title="file Upload" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="fileupload.aspx.cs" Inherits="fileupload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Repeater ID="rptUpload" runat="server">
      <HeaderTemplate>
      <table width="100%">
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:FileUpload ID="fu" runat="server" />
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>

    <p>
        <asp:ImageButton ID="btnUpload" runat="server" OnClick="btnUpload_Click" OnClientClick="javascript:document.forms[0].encoding = 'multipart/form-data';" />
    </p>

</asp:Content>

