﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;

public partial class SiteMaster : MasterPage
{
    int Roleid;
    string sPath;
    string b;
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;

    public HtmlGenericControl BodyTag
    {
        get
        {
            return MasterPageBodyTag;
        }
        set
        {
            MasterPageBodyTag = value;
        }
    }
  

    protected void Page_Init(object sender, EventArgs e)
    {
        // The code below helps to protect against XSRF attacks
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;
        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            // Use the Anti-XSRF token from the cookie
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            // Generate a new Anti-XSRF token and save to the cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
            {
                responseCookie.Secure = true;
            }
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;
    }

    void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            // Validate the Anti-XSRF token
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
               // throw new InvalidOperationException("Validation of Anti-XSRF token failed.");

                Response.Redirect("LoginS.aspx");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
       sPath= System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath);
       string[] substrings = sPath.Split('.');
       b= substrings[0];


       MasterPageBodyTag.Attributes.Add("class", b);
      // bVar.v = b;

 
       
        if (HttpContext.Current.Request.IsAuthenticated)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                AppUser oAU = (from ans in db.AppUsers 
                               where ans.sUserName == Context.User.Identity.Name.ToString()  
                               select ans).FirstOrDefault();

                if ((oAU != null))
                {
                    if (oAU.RoleID_FK == 1)
                    {
                        LinAdmin.Visible = true;
                        lnkAlldownloads.Visible = true;
                        LinkHome.Visible = false;
                        HyperLink1.Visible = true;
                        HyperLink2.Visible = true;
                        AdminUserList.Visible = false;
                        lnkStateDownloads.Visible = true;
                    }
                  else if(oAU.RoleID_FK == 2)
                    {
                        LinAdmin.Visible = true;
                        lnkAlldownloads.Visible = true;
                        LinkHome.Visible = false;
                        HyperLink1.Visible = true;
                        HyperLink2.Visible = true;
                        AdminUserList.Visible = true;
                        lnkStateDownloads.Visible = true;
                    }                        
                    else
                    {
                        LinAdmin.Visible = false;
                        lnkAlldownloads.Visible = false;
                        LinkHome.Visible = true;
                        HyperLink1.Visible = false;
                        HyperLink2.Visible = false;
                        AdminUserList.Visible = false;
                        lnkStateDownloads.Visible = false;
                    }
                }
            }

        }
              
    }

   

}