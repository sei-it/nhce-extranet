﻿<%@ Page Title="File Download" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AllFilesDownloads.aspx.cs" Inherits="AllFilesDownloads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Download All States Zipped Files</h2><br/>
   <div>
     <table>
          <tr>
              <th>Type</th>
              <th>Download</th>
          </tr>
           <tr>
              <td><h3>All State Workbooks</h3></td>
              <td><asp:Button ID="btnStateWorkbooks" runat="server" OnClick="btnStateWorkbooks_Click" Text="Download" /></td>
           </tr>
           <tr>           
              <td><h3>1.1 Monitoring and Evaluation of LEAs</h3></td>
              <td><asp:Button ID="Pre1" runat="server" OnClick="Pre1_Click" Text="Download" /></td>
           </tr>
           <tr>
              <td><h3>2.1 Identification, Enrollment, Retention through Coordination and Collaboration</h3></td>
              <td><asp:Button ID="Pre2" runat="server" OnClick="Pre2_Click" Text="Download" /></td>
           </tr>
           <tr>
              <td><h3>2.2 Technical Assistance to LEAs</h3></td>
              <td><asp:Button ID="Pre3" runat="server" OnClick="Pre3_Click" Text="Download" /></td>
           </tr>
           <tr>
              <td><h3>3.1 LEA Subgrants</h3></td>
              <td><asp:Button ID="Pre4" runat="server" OnClick="Pre4_Click" Text="Download" /></td>
           </tr>
           <tr>
              <td><h3>3.2 State-level Coordination Activities</h3></td>
              <td><asp:Button ID="Pre5" runat="server" OnClick="Pre5_Click" Text="Download" /></td>
           </tr>
           <tr>
             <td><h3>3.3 Dispute Resolution</h3></td>
              <td><asp:Button ID="Pre6" runat="server" OnClick="Pre6_Click" Text="Download" /></td>
           </tr>
           <tr>
              <td><h3>Data Workbooks Documents</h3></td>
              <td><asp:Button ID="DataWork" runat="server" OnClick="DataWork_Click" Text="Download" /></td>
           </tr>
           <tr>
              <td><h3>State Action Plans Documents</h3></td>
              <td><asp:Button ID="StateAction" runat="server" OnClick="StateAction_Click" Text="Download" /></td>
           </tr>
          
       </table>
       </div>
</asp:Content>

