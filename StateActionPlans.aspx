﻿<%@ Page Title="State Action Plans" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="StateActionPlans.aspx.cs" Inherits="StateActionPlans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
   
           <h2>State Action Plans Documents</h2>
    <span Class="backLink">
    <asp:LinkButton id="lnkBack"  Font-Bold="true"  Text="Back" runat="server" OnClick="lnkBack_Click"/></span>

    <div>
           <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="600px"
            DataKeyNames="StateActionPlansDocID" CssClass="etacTbl"  Style="margin-left: 8px;">
            <Columns>               
                <asp:TemplateField>
                     <HeaderTemplate>  
                         Uploaded file                    
                        </HeaderTemplate>  
                    <ItemTemplate>
                          <a target="_blank"  href='<%# String.Format("StateActionPlansDownload.aspx?PhysicalFileName={0}&StateActionPlansDocID={1}",Eval("PhysicalFileName"),Eval("StateActionPlansDocID"))%>'>
                                    <%# Eval("OriginalFileName")%></a>
                    </ItemTemplate>
                    <HeaderStyle width="550px" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton20" runat="server" Text="Delete" OnClientClick="return confirm('Are you certain you want to delete?');"
                            OnClick="LinkButton20_Click" CommandArgument='<%# Eval("StateActionPlansDocID")%>' />
                    </ItemTemplate>
                    <HeaderStyle width="50px" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
   

         <%--<h3>Upload State Action Plans</h3>--%>
    <div style="padding-left:8px" >
         <asp:FileUpload ID="FU8StateActionPlans" runat="server" CssClass="etacTxt" />
                    <asp:Button ID="btnFU8StateActionPlans" runat="server" Text="Upload" CssClass="etacBtn" ValidationGroup="Submit" OnClick="btnFU8StateActionPlans_Click"/>
                   <asp:Button ID="StateAction" runat="server" OnClick="StateAction_Click" Text="Download" />
         <asp:RegularExpressionValidator ID="regex8" runat="server"
                     ControlToValidate="FU8StateActionPlans" ErrorMessage="Only .jpg .pdf .docx .xlsx .gif .png .jpeg .doc .ppt files are allowed" 
                     ValidationExpression="(.*?)\.(jpg|pdf|docx|xlsx|gif|png|jpeg|doc|xls|PDF|PPT|ppt|pptx)$" ForeColor="Red" ValidationGroup="Submit">
                     </asp:RegularExpressionValidator><br />
        <asp:Label runat="server" id="StatusLabel"  ForeColor="Red" />
       
    </div>

   
   
    
</asp:Content>

