﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;
using System.Text;

public partial class Admin_AppUserEdit : System.Web.UI.Page
{
    int lngPkID;
    string newuser;
    string username;
    string user;
    string Firstname;
    string Lastname;
    int userId;
    string password;
    string url;
    UserInfo oUI;
    bool isDuplicateGc = false;
    DateTime dtTmp;
    protected void Page_Load(object sender, EventArgs e)
    {
        string strJS = null;

        if (!IsPostBack)
        {
            if (Page.Request.QueryString["UserID"] != null)
            {
                lngPkID = Convert.ToInt32(Page.Request.QueryString["UserID"]);
            }
            if (Page.Request.QueryString["newuser"] != null)
            {
                newuser = Page.Request.QueryString["newuser"];
            }
            
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            loadDropdown();

        }
       
        if (ViewState["IsLoaded1"] == null)
        {
            displayRecords();
           // SavebuttonEnable();
            ViewState["IsLoaded1"] = true;
        }
        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oPages = from pg in db.AppUsers
                         where pg.UserID == lngPkID
                         select pg;

            foreach (var oResSubmission in oPages)
            {               
                objVal = oResSubmission.FirstName;
                if (objVal != null)
                {
                    txtFirstName.Text = objVal.ToString();
                }

                objVal = oResSubmission.LastName;
                if (objVal != null)
                {
                    txtLastname.Text = objVal.ToString();
                }

                if (newuser == "newuser")
                {
                    objVal = oResSubmission.sUserName;
                    if (objVal != null)
                    {
                        txtUserID.Text = objVal.ToString();
                    }
                                  
                }
                else
                {
                    objVal = oResSubmission.sUserName;
                    if (objVal != null)
                    {
                        txtUserID.Text = objVal.ToString();
                    }
                    txtUserID.Enabled = false;
                    txtUserID.BackColor = System.Drawing.ColorTranslator.FromHtml("#F2F0E1");
                }

               

                objVal = oResSubmission.UserEmail;
                if (objVal != null)
                {
                    txtUserEmail.Text = objVal.ToString();
                }

                objVal = oResSubmission.RoleID_FK;
                if (objVal != null)
                {
                    if (oResSubmission.RoleID_FK == 3)
                    {
                        StateCor.Visible = true;
                        objVal = oResSubmission.StateID_FK;
                       if (objVal != null)
                        {
                            ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(oResSubmission.StateID_FK.ToString()));
                        }
                    }
                    rblAdminRole.SelectedIndex = rblAdminRole.Items.IndexOf(rblAdminRole.Items.FindByValue(oResSubmission.RoleID_FK.ToString()));
                }
            }

        }
    }
    private void loadDropdown()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            if (oUI.intUserRoleID == 1 || oUI.intUserRoleID == 2)                 // changed for SysAdmin role june27
            {
                var qry = from p in db.AppUserRoles 
                          select p;
                rblAdminRole.DataSource = qry;
                rblAdminRole.DataTextField = "RoleName";
                rblAdminRole.DataValueField = "UserRoleID";
                rblAdminRole.DataBind();
            }

            var qryS = from e in db.States
                       where e.StateFullName != null 
                       select e;
            ddlState.DataSource = qryS;
            ddlState.DataTextField = "StateFullName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();
            ddlState.Items.Insert(0, "");
        }
    }

    //----State view------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["lngPKID"] != null)))
        {
            lngPkID = Convert.ToInt32(this.ViewState["lngPKID"]);
        }

        if (((this.ViewState["newuser"] != null)))
        {
            newuser = Convert.ToString(this.ViewState["newuser"]);
        }

        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["lngPKID"] = lngPkID;
        this.ViewState["newuser"] = newuser;
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }
  

    //------Reset Password---------//
    protected void btnGeneratePassword_Click(object sender, EventArgs e)
    {
        lblErrorAdmin.Text = "";
        SendUserNameEmailNotification();
       // SendPasswordEmailNotification();
        ResetPasswordEmailNotification();
    }
   

    //-------Save, Close------------//
    protected void btnSave_Click(object sender, EventArgs e)
    {
        updateUser();            
        displayRecords();
    }    
    private void updateUser()
    {
        bool blNew = false;
        int strTmp;
        DateTime dtTmp;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
          
                AppUser oResSubmission = (from c in db.AppUsers where c.sUserName == txtUserID.Text select c).FirstOrDefault();

                if ((oResSubmission == null))
                {
                    oResSubmission = new AppUser();
                    blNew = true;
                }

                String result = Regex.Replace(txtUserID.Text, "^[ \t\r\n]+|[ \t\r\n]+$", "");

                // to check for duplicate user name
                if (newuser == "newuser")
                {
                    if (oResSubmission.sUserName == txtUserID.Text)
                    {
                        object objVal1 = null;
                        var oPages1 = from pg in db.AppUsers
                                      where pg.sUserName == result
                                      select pg;
                        foreach (var oCase1 in oPages1)
                        {
                            objVal1 = oCase1.sUserName;
                            if (objVal1.ToString() == result)
                            {
                                isDuplicateGc = true;
                                //string ErrorMsg = "User ID already exist";
                                //lblError.Text = ErrorMsg;
                            }
                            else
                            {
                                oResSubmission.sUserName = result;
                            }
                        }
                    }
                }
                else
                {
                    //object objVal1 = null;
                    //var oPages1 = from pg in db.AppUsers
                    //              where pg.sUserName == result
                    //              select pg;
                    //foreach (var oCase1 in oPages1)
                    //{
                    //    objVal1 = oCase1.sUserName;
                    //    if (objVal1.ToString() == result)
                    //    {
                    //        isDuplicateGc = true;
                    //        string ErrorMsg = "User ID already exist";
                    //        lblError.Text = ErrorMsg;
                    //    }
                    //    else
                    //    {
                    //        oResSubmission.sUserName = result;
                    //    }
                    //}

                }

               
                oResSubmission.FirstName = txtFirstName.Text;
                oResSubmission.LastName = txtLastname.Text;
                oResSubmission.UserEmail = txtUserEmail.Text;
                oResSubmission.sUserName = result;
                if (!(rblAdminRole.SelectedItem == null))
                {
                    if (!(rblAdminRole.SelectedItem.Value.ToString() == ""))
                    {
                        oResSubmission.RoleID_FK = Convert.ToInt32(rblAdminRole.SelectedItem.Value.ToString());
                       
                    }
                    else
                    {
                        oResSubmission.RoleID_FK = null;
                    }
                }

                if (!(ddlState.SelectedItem == null))
                {
                    if (!(ddlState.SelectedItem.Value.ToString() == ""))
                    {                      
                        oResSubmission.StateID_FK = Convert.ToInt32(ddlState.SelectedItem.Value.ToString());
                    }
                    else
                    {
                        oResSubmission.StateID_FK = null;
                    }
                }                                            

                if (blNew == true)
                {                    
                        oResSubmission.createdBy = oUI.sUserID;
                        oResSubmission.createdDt = DateTime.Now;
                        db.AppUsers.InsertOnSubmit(oResSubmission);
                        db.SubmitChanges();
                        lngPkID = oResSubmission.UserID;
                        SendUserNameEmailNotification();
                        SendPasswordEmailNotification();                    
                }
                else
                {
                    if (isDuplicateGc == false)
                    {
                        oResSubmission.updatedBy = oUI.sUserID;
                        oResSubmission.updatedDt = DateTime.Now;
                        db.SubmitChanges();
                        Adminrole.Visible = true;
                        lngPkID = oResSubmission.UserID;
                        litMessage.Text = "Updated!";
                    }
                    else
                    {
                        litMessage.Text = "User ID already exist";
                    }
                    
                }
            }        
     
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Redirect("UsersList.aspx");
    }

    //-------Create Random password--------------------//
    private string CreateRandomPassword(int passwordLength)
    {
        string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
        char[] chars = new char[passwordLength];
        Random rd = new Random();

        for (int i = 0; i < passwordLength; i++)
        {
            chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
        }

        return new string(chars);
    }


    //------ Email Notification---------------------//
    private void SendUserNameEmailNotification()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.UserID == lngPkID && c.sUserName == txtUserID.Text select c).FirstOrDefault();

            if ((oResSubmission != null))
            {
                Firstname = oResSubmission.FirstName;
                Lastname = oResSubmission.LastName;
                user = Firstname + ' ' + Lastname;
                username = oResSubmission.sUserName;
                userId = oResSubmission.UserID;               
               // db.SubmitChanges();
            }

            try
            {
                string strFrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
                string strTo = txtUserEmail.Text;
                MailMessage objMailMsg = new MailMessage(strFrom, strTo);
                //strTo = strTo.Replace("@seiservices.com", "@synergyE365.onmicrosoft.com");
                objMailMsg.CC.Add("ASeddoh@seiservices.com");
                objMailMsg.CC.Add("VKothale@seiservices.com");

               
                objMailMsg.BodyEncoding = Encoding.UTF8;
                objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSubject"];
                url = System.Web.Configuration.WebConfigurationManager.AppSettings["ncheSite"];


                //-------Email body-----------------------
                String strBody1;
                strBody1 = File.ReadAllText(Server.MapPath("UserNameConfirmation.html"));
                strBody1 = strBody1.Replace("[[User]]", user);
                strBody1 = strBody1.Replace("[[UserName]]", username);
                strBody1 = strBody1.Replace("[[url]]", url);
                
                objMailMsg.Body = strBody1;

                //--------prepare to send mail via SMTP transport-----//
                objMailMsg.Priority = MailPriority.High;
                objMailMsg.IsBodyHtml = true;

                //prepare to send mail via SMTP transport
                SmtpClient objSMTPClient = new SmtpClient();

                objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
                NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
                objSMTPClient.Send(objMailMsg);
                litMessage.Text = "User Name have been sent to " + oResSubmission.UserEmail;

            }
            catch (System.Exception ex)
            {
                litMessage.Text = "Could not send the e-mail - error: " + ex.Message;
            }
        }
    }
    private void SendPasswordEmailNotification()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.UserID == lngPkID && c.sUserName == txtUserID.Text select c).FirstOrDefault();

            if ((oResSubmission != null))
            {
                oResSubmission.Password = CreateRandomPassword(6);
                Firstname = oResSubmission.FirstName;
                Lastname = oResSubmission.LastName;
                username = Firstname + ' ' + Lastname;
                userId = oResSubmission.UserID;
                password = oResSubmission.Password;
                db.SubmitChanges();
            }

            try
            {
                string strFrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
                string strTo = txtUserEmail.Text;
                MailMessage objMailMsg = new MailMessage(strFrom, strTo);
                //strTo = strTo.Replace("@seiservices.com", "@synergyE365.onmicrosoft.com");
                objMailMsg.CC.Add("ASeddoh@seiservices.com");
                objMailMsg.CC.Add("VKothale@seiservices.com");
               
                objMailMsg.BodyEncoding = Encoding.UTF8;
                objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSubject"];
                url = System.Web.Configuration.WebConfigurationManager.AppSettings["ncheSite"];


                //-------Email body-----------------------
                String strBody1;
                strBody1 = File.ReadAllText(Server.MapPath("AccountEmailConfirmation.html"));
                strBody1 = strBody1.Replace("[[User]]", user);
                strBody1 = strBody1.Replace("[[password]]", password);
                strBody1 = strBody1.Replace("[[url]]", url);
                objMailMsg.Body = strBody1;

                //--------prepare to send mail via SMTP transport-----//
                objMailMsg.Priority = MailPriority.High;
                objMailMsg.IsBodyHtml = true;

                //prepare to send mail via SMTP transport
                SmtpClient objSMTPClient = new SmtpClient();

                objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
                NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
                objSMTPClient.Send(objMailMsg);
                litMessage.Text = "Your password has been sent to the above email.";

            }
            catch (System.Exception ex)
            {
                litMessage.Text = "Could not send the e-mail - error: " + ex.Message;
            }
        }
    }
    private void ResetPasswordEmailNotification()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            AppUser oResSubmission = (from c in db.AppUsers where c.UserID == lngPkID && c.sUserName == txtUserID.Text select c).FirstOrDefault();

            if ((oResSubmission != null))
            {
                oResSubmission.Password = CreateRandomPassword(6);
                Firstname = oResSubmission.FirstName;
                Lastname = oResSubmission.LastName;
                username = Firstname + ' ' + Lastname;
                userId = oResSubmission.UserID;
                password = oResSubmission.Password;
                db.SubmitChanges();
            }

            try
            {
                string strFrom = System.Web.Configuration.WebConfigurationManager.AppSettings["NotificationSenderAddress"];
                string strTo = txtUserEmail.Text;
                MailMessage objMailMsg = new MailMessage(strFrom, strTo);
                //strTo = strTo.Replace("@seiservices.com", "@synergyE365.onmicrosoft.com");
                objMailMsg.CC.Add("ASeddoh@seiservices.com");
                objMailMsg.CC.Add("VKothale@seiservices.com");

                objMailMsg.BodyEncoding = Encoding.UTF8;
                objMailMsg.Subject = System.Web.Configuration.WebConfigurationManager.AppSettings["EmailSubject"];
                url = System.Web.Configuration.WebConfigurationManager.AppSettings["ncheSite"];


                //-------Email body-----------------------
                String strBody1;
                strBody1 = File.ReadAllText(Server.MapPath("ResetEmailConfirmation.html"));
                strBody1 = strBody1.Replace("[[User]]", user);
                strBody1 = strBody1.Replace("[[password]]", password);
                strBody1 = strBody1.Replace("[[url]]", url);
                objMailMsg.Body = strBody1;

                //--------prepare to send mail via SMTP transport-----//
                objMailMsg.Priority = MailPriority.High;
                objMailMsg.IsBodyHtml = true;

                //prepare to send mail via SMTP transport
                SmtpClient objSMTPClient = new SmtpClient();

                objSMTPClient.Host = System.Web.Configuration.WebConfigurationManager.AppSettings["SmtpServer"];
                NetworkCredential userCredential = new NetworkCredential("SEInfo@seiservices.com", "");
                objSMTPClient.Send(objMailMsg);
                litMessage.Text = "Your password has been sent to the above email.";

            }
            catch (System.Exception ex)
            {
                litMessage.Text = "Could not send the e-mail - error: " + ex.Message;
            }
        }
    }

    protected void rblAdminRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        
            if (!(rblAdminRole.SelectedItem.Value.ToString() == ""))
            {
                if (rblAdminRole.SelectedItem.Value.ToString() == "3")
                {
                    StateCor.Visible = true;
                    rfvState.Enabled = true;
                }
                else
                {
                    StateCor.Visible = false;
                    rfvState.Enabled = false;
                    ddlState.SelectedIndex = 0;
                }
            }
        
    }
}