﻿<%@ Page Title="LEA data" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="LEAdata.aspx.cs" Inherits="LEAdata" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>LEA CSPR Longitudinal Data</h2>
<div class="main-content">
    <p style="font-size:15px" > <asp:HyperLink NavigateUrl="LEA_Enrollment-1.aspx" runat="server" Visible="true" ID="LinkEnr" ForeColor="Blue" Text="Enrolled LEA School Data"></asp:HyperLink></p>
    <p style="font-size:15px"> <asp:HyperLink NavigateUrl="LEA_Served-1.aspx" runat="server" Visible="true" ID="LinSer" ForeColor="Blue" Text="Served LEA School Data"></asp:HyperLink></p>
    <p style="font-size:15px"> <asp:HyperLink NavigateUrl="LEA_MathAssessment.aspx" runat="server" Visible="true" ID="lnkMath" ForeColor="Blue" Text="Math Assessment LEA Data"></asp:HyperLink></p>
    <p style="font-size:15px"> <asp:HyperLink NavigateUrl="LEA_ScienceAssessment.aspx" runat="server" Visible="true" ID="LinScie" ForeColor="Blue" Text="Science Assessment LEA Data"></asp:HyperLink></p>
    <p style="font-size:15px"> <asp:HyperLink NavigateUrl="LEA_EnglishAssessment.aspx" runat="server" Visible="true" ID="LinEng" ForeColor="Blue" Text="ELA Assessment LEA Data"></asp:HyperLink></p>
    </div>  
</asp:Content>

