﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;

public partial class All_LEAEnrollment : System.Web.UI.Page
{
    string SchoolYear = "";
    string StateName = "";
    string sName;
    string sYear;   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            if (Page.Request.QueryString["SchooYear"] != null)
            {
                SchoolYear = Page.Request.QueryString["SchooYear"].ToString();
            }
            if (Page.Request.QueryString["StateName"] != null)
            {
                StateName = Page.Request.QueryString["StateName"].ToString();
            }

           // DynamicGridView();
           
        }
        CreateGridView();

        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void DynamicGridView()
    {
        ReportData rep1 = new ReportData();
        DataSet ds = new DataSet();
        ds = rep1.Get_AllSEA_Math(StateName, SchoolYear, "spLEA_Enrolled_LEA");
        CreateGridView(ds);
    }
    //-----View State------------------------//
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["SchoolYear"] != null)))
        {
            SchoolYear = this.ViewState["SchoolYear"].ToString();
        }
        if (((this.ViewState["StateName"] != null)))
        {
            StateName = this.ViewState["StateName"].ToString();
        }
        
    }
    protected override object SaveViewState()
    {

        
        this.ViewState["allchecked"] = SchoolYear;
        this.ViewState["TopicFreez"] = StateName;

        return (base.SaveViewState());
    }

    //Load dropdow and radiobuttonlist data--//
   
    //private void displayRecords()
    //{
    //    EnrData.Visible = true;
    //    divMessage.Text = "";
       
    //    ReportData rep1 = new ReportData();
    //    DataSet ds = new DataSet();
    //    ds = rep1.Get_AllSEAEnrolled_search(SchoolYear, StateName, "spLEA_Enrolled_LEA");
    //    CreateGridView(ds);
    //}
    //--------------------------------------//

    //--Create Dynamic grid view--//
    private void CreateGridView(DataSet ds)
    {
        //PlaceHolder1.Controls.Remove(gv);
        ReportData rep1 = new ReportData();
        DataTable dt = new DataTable();
        // ds = rep1.Get_AllSEAEnrolled_search(SchoolYear, StateName, "EnrolledAbb_SY11_14");
        dt = ds.Tables[0];

        GridView gv = new GridView();
        gv.ID = "LEAGridView";
        gv.AutoGenerateColumns = true;
        gv.AllowPaging = false;
        gv.EnableViewState = true;
        // gv.PageSize = 10; // Default page Size

        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    BoundField boundField = new BoundField();
                    boundField.DataField = dt.Columns[i].ColumnName.ToString();
                    boundField.HeaderText = dt.Columns[i].ColumnName.ToString();
                    gv.Columns.Add(boundField);
                }
            }
        }
        PlaceHolder1.Controls.Add(gv);
        BindGridView(gv, dt);
    }
    private void CreateGridView()
    {

        ReportData rep1 = new ReportData();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        ds = rep1.Get_AllSEAEnrolled_search(SchoolYear, StateName, "spLEA_Enrolled_LEA");
        dt = ds.Tables[0];

        GridView gv = new GridView();
        gv.ID = "LEAGridView";
        gv.AutoGenerateColumns = false;
        gv.AllowPaging = false;
        gv.EnableViewState = false;
        // gv.PageSize = 10; // Default page Size
        // PlaceHolder1.Controls.Remove(gv);
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    BoundField boundField = new BoundField();
                    boundField.DataField = dt.Columns[i].ColumnName.ToString();
                    boundField.HeaderText = dt.Columns[i].ColumnName.ToString();
                    gv.Columns.Add(boundField);
                }
            }
        }
        PlaceHolder1.Controls.Add(gv);
        BindGridView(gv, dt);

    }
    private void BindGridView(GridView gv, DataTable dt)
    {
        gv.DataSource = dt;
        gv.DataBind();

    }  
    //--------------------------//   


    //---Data export in excel-----//
    protected void lnkData_Click(object sender, EventArgs e)
    {
        
        Get_ExcelData();
    }
    private void Get_ExcelData()
    {
        ReportData rep = new ReportData();
        if (SchoolYear != null && SchoolYear != "")
        {
            if (SchoolYear != "2014-2015")
            {
                // DataSet ds = rep.Get_AllSEAEnrolled(StateName, "spE_SEAEnrolled_SY11_12");
                DataSet ds = rep.Get_AllSEAEnrolled_search(SchoolYear, StateName, "spLEA_Enrolled_LEA");
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    ExportToExcel(ds, 0, Response, "Report");
                }
            }
            else
            {
                EnrData.Visible = false;
                divMessage.Text = "LEA Enrolled data not found for School Year 2014-15";
            }
        }
        else
        {
            DataSet ds = rep.Get_AllSEAEnrolled_search(SchoolYear, StateName, "spLEA_Enrolled_LEA");
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExportToExcel(ds, 0, Response, "Report");
            }
        }

    }
    private void ExportToExcel(DataSet ds, int TableIndex, HttpResponse Response, string FileName)
    {
        int i, j;
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        // Response.ContentType = "application/vnd.ms-excel";
        string timestamp = Convert.ToString(DateTime.Now);
        timestamp = timestamp.Replace(" ", "_");
        timestamp = timestamp.Replace("/", "_");
        //  timestamp = timestamp.Replace(":", ":");
        FileName = FileName.Replace(" ", "");

        //string ExtractName = "Enrolled" + "(" + timestamp + ")" + FileName + " .xlsx";
        string ExtractName = "All_LEAEnrolled_" + FileName + " .xlsx";
        //  Response.AppendHeader("content-disposition", "attachment; filename=" + ExtractName + ".xls");

        string sTempFileName = Server.MapPath(@"~\temp\dataeX_") + Guid.NewGuid().ToString() + ".xlsx";

        ExportDataSet(ds, sTempFileName);
        displayExport(sTempFileName, ExtractName);

    }
    public static void ExportDataSet(DataSet ds, string destination)
    {
        using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
        {
            var workbookPart = workbook.AddWorkbookPart();

            workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

            foreach (System.Data.DataTable table in ds.Tables)
            {

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                uint sheetId = 1;
                if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                {
                    sheetId =
                        sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }

                DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }


                sheetData.AppendChild(headerRow);

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (String col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        dynamic dc = dsrow[col];
                        if (dc.GetType().ToString() == "System.Boolean")
                        {
                            if (dsrow[col].ToString() == "True")
                            {
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Yes");
                            }

                        }
                        else
                        {
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString());
                        }
                        //if (Type.GetType(dc.GetType().ToString()))
                        //
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

            }
        }
    }
    private void displayExport(string sTempFileName, string sFilename)
    {
        try
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.spreadsheet";
            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);
            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);

            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);

            }
        }
    }
    //-----------------------------//
    
}