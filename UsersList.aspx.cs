﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;

public partial class Admin_UsersList : System.Web.UI.Page
{
    UserInfo oUI;
    string UserName;
    int Userid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            oUI = Auth.getUserInfo(HttpContext.Current.User.Identity.Name);
            if (!(oUI.intUserRoleID == 2 || oUI.intUserRoleID == 1)) // changed for SysAdmin role june27)
            {
               // Response.Redirect("~/Error/UnAuthorized.aspx");
                Response.Redirect("LoginS.aspx");
            }

            displayRecords();
            ViewState["IsLoaded1"] = true;
        }
        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void displayRecords()
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
           
            var vUsers = from p in db.AppUsers                        
                         select p;
            grdVwList.DataSource = vUsers;
            grdVwList.DataBind();
        }

    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["oUI"] != null)))
        {
            oUI = (UserInfo)(this.ViewState["oUI"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["oUI"] = oUI;
        return (base.SaveViewState());
    }
    protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            int intID = Convert.ToInt32(e.CommandArgument);
           
            Response.Redirect("AppUserEdit.aspx?UserID=" + intID );
        }
        
        if (e.CommandName == "Delete")
        {
            int intID = Convert.ToInt32(e.CommandArgument);
            DeleteAdminUser(intID);
            Response.Redirect("UsersList.aspx");
        }
    }

    private void DeleteAdminUser(int intID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var case0 = (from c in db.AppUsers
                         where c.UserID == intID
                         select new { c.sUserName });

            if ((case0 != null))
            {
                foreach (var var1 in case0)
                {
                    if (!(var1.sUserName == null))
                    {
                        UserName = var1.sUserName.ToString();
                    }
                }
            }

            AppUser case1 = (from c in db.AppUsers
                             where c.UserID == intID
                             select c).FirstOrDefault();
            if ((case1 != null))
            {
                db.AppUsers.DeleteOnSubmit(case1);
            }
            db.SubmitChanges();

        }     
    }
    protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        LinkButton imgBtnDelete;
        LinkButton imgBtnViewEdit;
        LinkButton imgBtnEdit;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "Userid") != null)
            {
                Userid = (int)DataBinder.Eval(e.Row.DataItem, "Userid");
            }

            //if (oUI.intUserRoleID == 3)
            //{
            //    imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
            //    imgBtnDelete.Visible = true;
            //    imgBtnDelete.Attributes.Add("onclick", "javascript:return " +
            //                                  "confirm('Are you sure you want to delete this record')");

            //    imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
            //    imgBtnEdit.Visible = true;
            //}
            //else if (oUI.intUserRoleID == 4)
            //{
            //    imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
            //    imgBtnDelete.Visible = false;

            //    imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
            //    imgBtnEdit.Visible = true;
            //}
            //else
            //{
            //    imgBtnDelete = (LinkButton)e.Row.FindControl("imgBtnDelete");
            //    imgBtnDelete.Visible = false;

            //    imgBtnEdit = (LinkButton)e.Row.FindControl("imgBtnEdit");
            //    imgBtnEdit.Visible = false;
            //}

        }
    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        string newuser = "newuser";
        int intAppUserID=0;
        Response.Redirect("AppUserEdit.aspx?intAppUserID=" + intAppUserID + "&newuser=" + newuser);
       // Response.Redirect("AppUserEdit.aspx?intAppUserID=0");
    }
}