﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class SEAdata : System.Web.UI.Page
{
    string Years;
    int reportID;
    protected void Page_Load(object sender, EventArgs e)
    {
        //int StateID;
        //if (!IsPostBack)
        //{
        //    string currentDate = DateTime.Today.ToShortDateString();

        //    if (Page.Request.QueryString["lngPkID"] != null)
        //    {
        //        StateID = Convert.ToInt32(Page.Request.QueryString["lngPkID"]);
        //    }
        //}
        //loadData();
        //displayRecords();
        //Page.MaintainScrollPositionOnPostBack = true;
    }

    //private void displayRecords()
    //{
    //    ReportData rep1 = new ReportData();
    //    DataSet ds = new  DataSet();
    //    ds = rep1.Get_AllSEAEnrolled("get_SEAEnrolled_SY11_14");
    //    grdVwList.DataSource = ds;
    //    grdVwList.DataBind();

    //    DataSet dsserved = new DataSet();
    //    dsserved = rep1.Get_AllSEAServed("get_SEAServed_SY11_14");
    //    GridViewServed.DataSource = dsserved;
    //    GridViewServed.DataBind();


    //}

    //private void loadData()
    //{
    //    using (DataClassesDataContext db = new DataClassesDataContext())
    //    {
    //        var qry = from p in db.SchoolYears
    //                  where p.SYearID>7
    //                   select p ;
    //        chkSchoolyear.DataSource = qry;
    //        chkSchoolyear.DataTextField = "SchoolYear1";
    //        chkSchoolyear.DataValueField = "SYearID";
    //        chkSchoolyear.DataBind();

    //        var qryS = from e in db.States
    //                   where e.StateFullName!= null
    //                  select e;
    //        ddlState.DataSource = qryS;
    //        ddlState.DataTextField = "StateFullName";
    //        ddlState.DataValueField = "StateID";
    //        ddlState.DataBind();
    //        ddlState.Items.Insert(0, "Select");

    //        var qryC = from e in db.ReportTypes
    //                   select e;
    //        ddlReportType.DataSource = qryC;
    //        ddlReportType.DataTextField = "ReportTypeName";
    //        ddlReportType.DataValueField = "TypeID";
    //        ddlReportType.DataBind();
    //        ddlReportType.Items.Insert(0, "Select");
            

    //    }
    //}
    //protected void grdVwList_RowCommand(object sender, GridViewCommandEventArgs e)
    //{

    //}
    //protected void grdVwList_RowDataBound(object sender, GridViewRowEventArgs e)
    //{

    //}
   
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        reportID = 4;
        Response.Redirect("PreFilledReport.aspx?reportID=" + reportID);
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        reportID = 3;
        Response.Redirect("PreFilledReport.aspx?reportID=" + reportID);
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        reportID = 2;
        Response.Redirect("PreFilledReport.aspx?reportID=" + reportID);
    }    
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        reportID = 1;
        Response.Redirect("PreFilledReport.aspx?reportID=" + reportID);
    }
    protected void lnkTotalHomeless_Click(object sender, EventArgs e)
    {
        reportID = 1;
        Response.Redirect("PreFilledReport.aspx?reportID="+ reportID);

    }
    protected void lnkHomelessGraduation_Click(object sender, EventArgs e)
    {
        reportID = 2;
        Response.Redirect("PreFilledReport.aspx?reportID=" + reportID);

    }
    protected void lnkHCYProfMathematics_Click(object sender, EventArgs e)
    {
        reportID = 3;
        Response.Redirect("PreFilledReport.aspx?reportID=" + reportID);
    }
    protected void InkHCYProfELA_Click(object sender, EventArgs e)
    {
        reportID = 4;
        Response.Redirect("PreFilledReport.aspx?reportID=" + reportID);
    }
}