﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using ICSharpCode.SharpZipLib.Zip;
using System.Configuration;


public partial class StateActionPlans : System.Web.UI.Page
{
    int StateID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["StateID"] != null)
            {
                StateID = Convert.ToInt32(Page.Request.QueryString["StateID"]);
            }
           // loadGridview2(StateID);
        }
        loadGridview(StateID);
        
        Page.MaintainScrollPositionOnPostBack = true;
    }

    

    private void loadGridview(int StateID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oAU = from ans in db.StateActionPlansUploads
                      where  ans.StateID_fk == StateID
                      select ans;

            GridView1.DataSource = oAU;
            GridView1.DataBind();

        }

    }
    protected void btnFU8StateActionPlans_Click(object sender, EventArgs e)
    {
        if (FU8StateActionPlans.HasFile)
        {
            using (DataClassesDataContext db = new DataClassesDataContext())
            {
                try
                {
                    string newFileName = Guid.NewGuid().ToString() + "-" + FU8StateActionPlans.FileName;  
                   // string newFileName = FU8StateActionPlans.FileName;
                    //FU8StateActionPlans.SaveAs(Server.MapPath("~/StateActionDocs/") + newFileName);
                    //string Filepath = "~/StateActionDocs/" + newFileName; 

                    string sourceFile = ConfigurationManager.AppSettings["StateAction"].ToString();
                    FU8StateActionPlans.SaveAs(sourceFile + newFileName);
                    string Filepath = sourceFile + newFileName;

                    StateActionPlansUpload attachment = (from c in db.StateActionPlansUploads select c).FirstOrDefault();
                    attachment = new StateActionPlansUpload();
                    attachment.OriginalFileName = FU8StateActionPlans.FileName;
                    attachment.PhysicalFileName = newFileName;
                    attachment.CreatedBy = User.Identity.Name.ToString();
                    attachment.CreatedDate = DateTime.Now;
                    attachment.StateID_fk = StateID;
                    attachment.FilePath = Filepath;
                    db.StateActionPlansUploads.InsertOnSubmit(attachment);
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    StatusLabel.Text = " Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
                StatusLabel.Text = "File has successfully uploaded.";
            }
        }
        loadGridview(StateID);
        Response.Redirect("StateActionPlans.aspx?StateID=" + StateID);
    }
    protected void LinkButton20_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            string id = (sender as LinkButton).CommandArgument;
           
            StateActionPlansUpload oAU = (from ans in db.StateActionPlansUploads where ans.StateActionPlansDocID == Convert.ToInt32(id) select ans).FirstOrDefault();

            //if (File.Exists(Server.MapPath("~/StateActionDocs/") + oAU.PhysicalFileName))
            //{
            //    File.Delete(Server.MapPath("~/StateActionDocs/") + oAU.PhysicalFileName);
            //}

            string sourceFile = ConfigurationManager.AppSettings["StateAction"].ToString();
            if (File.Exists(sourceFile + oAU.PhysicalFileName))
            {
                File.Delete(sourceFile + oAU.PhysicalFileName);
            }
            
            if ((oAU != null))
            {
                db.StateActionPlansUploads.DeleteOnSubmit(oAU);
            }
            db.SubmitChanges();
            loadGridview(StateID);
            Response.Redirect("StateActionPlans.aspx?StateID=" + StateID);
        }     
      
    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["StateID"] != null)))
        {
            StateID = Convert.ToInt32(this.ViewState["StateID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["StateID"] = StateID;
        return (base.SaveViewState());
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("StateDocList.aspx?StateID=" + StateID);
    }
    protected void StateAction_Click(object sender, EventArgs e)
    {
        List<ListItem> files = new List<ListItem>();
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.StateActionPlansUploads
                      where ans.StateID_fk == StateID
                      select ans;

           // string[] filePaths = Directory.GetFiles(Server.MapPath("~/StateActionDocs/"));
            string sourceFile = ConfigurationManager.AppSettings["StateAction"].ToString();
            string[] filePaths = Directory.GetFiles(sourceFile);

            foreach (string filePath in filePaths)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        files.Add(new ListItem(Path.GetFileName(filePath), filePath));
                    }
                }
            }
        }
        //string Filename = "StateActionPlansDocs.zip";
        string Filename = String.Format("StateActionPlansDocs_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
        ZipFilesDownload(Filename, files);
    }
    private void ZipFilesDownload(string zipFileName, List<ListItem> files)
    {
        //// Here we will create zip file & download    
        Response.ContentType = "application/zip";
        Response.AddHeader("content-disposition", "fileName=" + zipFileName);

        using (var zipStream = new ZipOutputStream(Response.OutputStream))
        {

            foreach (ListItem item in files)
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(item.Value);

                var fileEntry = new ZipEntry(Path.GetFileName(item.Value).Substring(37))
                {
                    Size = fileBytes.Length
                };

                zipStream.PutNextEntry(fileEntry);
                zipStream.Write(fileBytes, 0, fileBytes.Length);
            }

            zipStream.Flush();
            zipStream.Close();
        }

    }
   
    
}