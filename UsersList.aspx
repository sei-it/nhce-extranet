﻿<%@ Page Title="User List" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="UsersList.aspx.cs" Inherits="Admin_UsersList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>User List</h2>
    <h2>
        <asp:Literal ID="litCaseID" runat="server"></asp:Literal></h2>
    <div>
        <asp:Button ID="btnNew" runat="server" Text="Create New User" OnClick="btnNew_Click" />
    </div>
    <p></p>

    <asp:GridView ID="grdVwList" runat="server" CssClass="gridTbl"  AutoGenerateColumns="False"
        AllowSorting="True" OnRowCommand="grdVwList_RowCommand" OnRowDataBound="grdVwList_RowDataBound">
        <Columns>

            <asp:BoundField DataField="StateID_FK" HeaderText="State ID" DataFormatString="{0:g}">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>

            <asp:BoundField DataField="sUserName" HeaderText="UserID" DataFormatString="{0:g}">
                <HeaderStyle Width="150px"></HeaderStyle>
            </asp:BoundField>

             <asp:BoundField DataField="UserEmail" HeaderText="User Email" DataFormatString="{0:g}">
                <HeaderStyle Width="250px"></HeaderStyle>
            </asp:BoundField>

            <asp:BoundField DataField="FirstName" HeaderText="First Name" DataFormatString="{0:g}">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>

            <asp:BoundField DataField="LastName" HeaderText="Last Name" DataFormatString="{0:g}">
                <HeaderStyle Width="100px"></HeaderStyle>
            </asp:BoundField>

            <asp:TemplateField>
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnEdit" runat="server" CommandName="View" Visible="true"
                        CommandArgument='<%# Eval("Userid") %>'>View/Edit</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>

            
            <asp:TemplateField>
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="imgBtnDelete" runat="server" OnClientClick="return confirm('Are you sure you want to delete this User?');" CommandName="Delete" Visible="true" CommandArgument='<%# Eval("Userid") %>'>Delete</asp:LinkButton>

                </ItemTemplate>
                <HeaderStyle Width="100px" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

