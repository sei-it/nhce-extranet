﻿<%@ Page Title="App User Edit" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AppUserEdit.aspx.cs" Inherits="Admin_AppUserEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
  
    <h2>User Edit</h2>
    <div class="formLayout">
        <p>
            <asp:Label ID="lblError" runat="server" ForeColor="Red" Text=""></asp:Label>

        </p>
        <p>
            <label for="txtFirstName" style="padding-right:10px">
                First Name: <span style="color:red">*</span>      
            </label>
            <asp:TextBox ID="txtFirstName" runat="server" Width="225px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtFirstName" ValidationGroup="Save"
                ErrorMessage="First Name" ForeColor="Red">Required!</asp:RequiredFieldValidator>
        </p>

        <p>
            <label for="txtLastname" style="padding-right:14px">
                Last Name: <span style="color:red">*</span>
            </label>
            <asp:TextBox ID="txtLastname" runat="server" Width="225px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="txtLastname" ValidationGroup="Save"
                ErrorMessage="Last Name" ForeColor="Red">Required!</asp:RequiredFieldValidator>
        </p>

        <p>
            <label for="txtUserID" style="padding-right:36px">
                User ID: <span style="color:red">*</span>           
            </label>
            <asp:TextBox ID="txtUserID" runat="server" Width="225px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvtxtUserNames" runat="server" ControlToValidate="txtUserID" ValidationGroup="Save"
                ErrorMessage="User Name " ForeColor="Red">Required!</asp:RequiredFieldValidator>

            <span id="duplicate" style="color: #FF0000"></span>

        </p>


        <p>
            <label for="txtUserEmail" style="padding-right:52px">
                Email: <span style="color:red">*</span>
          
            </label>
            <asp:TextBox ID="txtUserEmail" runat="server" Width="225px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvtxtUserEmail" runat="server" ControlToValidate="txtUserEmail" ValidationGroup="Save"
                ErrorMessage="Email Address" ForeColor="Red">Required!</asp:RequiredFieldValidator>

            <span id="Span1" style="color: #FF0000"></span>

        </p>
        <div id="Adminrole" runat="server" >
            <p>

                <label for="rblAdminrole">Admin Role: <span style="color:red">*</span>
                
                </label>&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Save"
                    ControlToValidate="rblAdminRole" ErrorMessage="Admin Role" ForeColor="Red">Required!</asp:RequiredFieldValidator>

                <asp:RadioButtonList ID="rblAdminRole" AutoPostBack="true" OnSelectedIndexChanged="rblAdminRole_SelectedIndexChanged" runat="server" RepeatDirection="Horizontal">
                
          </asp:RadioButtonList>
                      </p>

        </div>

         <div id="StateCor" visible="false" runat="server" >
            <p>

                <label for="ddlState">
                    Select State: <span style="color:red">*</span>               
                </label>
               <asp:DropDownList ID="ddlState" Visible="true" runat="server"></asp:DropDownList>
                &nbsp;<asp:RequiredFieldValidator ID="rfvState" Enabled="false" runat="server" ValidationGroup="Save"
                    ControlToValidate="ddlState" ErrorMessage="Select State" ForeColor="Red">Required!</asp:RequiredFieldValidator>
            </p>

        </div>

        <p>
            <asp:Label ID="lblErrorAdmin" runat="server" ForeColor="Red" test=""></asp:Label>
        </p>

        <p>
            <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;      
        <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" />&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnGeneratePassword" runat="server" ValidationGroup="Save" Text="Reset Password" OnClick="btnGeneratePassword_Click" />

        </p>
        <p>
            <asp:Label ID="litMessage" runat="server" ForeColor="Red" test=""></asp:Label>
          </p>
        <p>
            &nbsp;<asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Save" runat="server" ForeColor="Red" HeaderText="Please fill in all the required fields." />

    </div>
</asp:Content>

