﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using ICSharpCode.SharpZipLib.Zip;
using System.Configuration;

public partial class PMonitoringEvaldownload1 : System.Web.UI.Page
{
    string PhysicalFileName;
    string PhysicalFileNamePath;
    int Pre_MonitoringID_FK;
    int PremonitoringDocID;
    bool forceDownload = true;
    string filePaths;
    int intFileID;
    int Stateid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["PhysicalFileName"] != null)
            {
                PhysicalFileNamePath = Page.Request.QueryString["PhysicalFileName"];
                PremonitoringDocID = Convert.ToInt32(Page.Request.QueryString["PremonitoringDocID"]);
                Pre_MonitoringID_FK = Convert.ToInt32(Page.Request.QueryString["Pre_MonitoringID_FK"]);

                if (Pre_MonitoringID_FK == 1)
                {
                   // filePaths = "~/PMonitoringEval/";
                    filePaths = ConfigurationManager.AppSettings["Premonitoring1.1"].ToString();
                }
                else if (Pre_MonitoringID_FK == 2)
                {
                   // filePaths = "~/PStateLocalAgencyPR/";
                    filePaths = ConfigurationManager.AppSettings["Premonitoring2.1"].ToString();
                }
                else if (Pre_MonitoringID_FK == 3)
                {
                    //filePaths = "~/PFiscalOversight/";
                    filePaths = ConfigurationManager.AppSettings["Premonitoring2.2"].ToString();
                }
                else if (Pre_MonitoringID_FK == 4)
                {
                    //filePaths = "~/PMcKinneyMonitoringEval/";
                    filePaths = ConfigurationManager.AppSettings["Premonitoring3.1"].ToString();
                }
                else if (Pre_MonitoringID_FK == 5)
                {
                    //filePaths = "~/PMcKinneyVentoProgS/";
                    filePaths = ConfigurationManager.AppSettings["Premonitoring3.2"].ToString();
                }
                else if (Pre_MonitoringID_FK == 6)
                {
                    //filePaths = "~/PMcKinneyVentoFiscalO/";
                    filePaths = ConfigurationManager.AppSettings["Premonitoring3.3"].ToString();

                }

                PhysicalFileName = filePaths + PhysicalFileNamePath;
                if (IsFIleAllowed(PhysicalFileName, PremonitoringDocID))
                {
                    // displayDocument(intFileID);
                    DownloadFile(PhysicalFileName, forceDownload, PremonitoringDocID);
                }
                else
                {

                    Response.Write("<script language='javascript'> { window.close(); }</script>");
                    // Page.ClientScript.RegisterStartupScript(this.GetType(), "myCloseScript", "window.close()", true); 
                    //ErrorMess.Text = "Please click on home tab to download document";
                }

            }
        }

    }

    private bool IsFIleAllowed(string PhysicalFileName, int PremonitoringDocID)
    {
        bool blIsValid = false;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var vSId = from s in db.PremonitoringUploads
                       where s.PhysicalFileName == PhysicalFileNamePath && s.PremonitoringDocID == PremonitoringDocID
                       select s;
            foreach (var value in vSId)
            {
                if (value.StateID_fk != null)
                {
                    Stateid = Convert.ToInt32(value.StateID_fk);
                }
            }

            AppUser oAU = (from ans in db.AppUsers where ans.sUserName == User.Identity.Name.ToString() select ans).FirstOrDefault();

            // var oFileRecs = db.IsFileAllowedForUser(User.Identity.Name.ToString(), Stateid).ToList();
            StateDoc oFileRecs = (from c in db.StateDocs
                                  join Appu in db.AppUsers
                                  on c.StateID_fk equals Appu.StateID_FK
                                  where c.StateID_fk == Stateid && Appu.sUserName == User.Identity.Name.ToString()
                                  select c).FirstOrDefault();




            if (oAU.RoleID_FK == 1 || oAU.RoleID_FK == 2)
            {
                blIsValid = true;
            }
            else
            {
                if (oFileRecs != null)
                {
                    blIsValid = true;
                }
                else
                {
                    blIsValid = false;
                }
            }
        }

        return blIsValid;
    }

    private void DownloadFile(string fname, bool forceDownload, int PremonitoringDocID)
    {
       // string path = MapPath(fname);

        string name1 = Path.GetFileName(fname);
        string name = name1.Substring(37);
        string ext = Path.GetExtension(fname);
        string type = "";
        // set known types based on file extension  
        if (ext != null)
        {
            switch (ext.ToLower())
            {
                case ".htm":
                case ".html":
                    type = "text/HTML";
                    break;

                case ".txt":
                    type = "text/plain";
                    break;

                case ".doc":
                case ".docx":
                case ".rtf":
                    type = "Application/msword";
                    break;

                case ".xls":
                case ".xlsx":
                    type = "Application/x-msexcel";
                    break;

                case ".pdf ":
                    type = "Application/pdf";
                    break;

                case ".gif ":
                    type = "image/GIF";
                    break;

                case ".png ":
                    type = "image/png";
                    break;

                case ".jpg":
                case ".jpeg":
                    type = "image/jpeg";
                    break;


                case ".ppt":
                case ".pptx":
                    type = "Application/powerpoint";
                    break;

            }
        }
        if (forceDownload)
        {
            Response.AppendHeader("content-disposition",
                "attachment; filename=" + name);
        }
        if (type != "")
            Response.ContentType = type;
        Response.WriteFile(fname);
        Response.End();
    }

}