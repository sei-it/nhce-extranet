﻿<%@ Page Title="File download Error" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AllFilesdownloadError.aspx.cs" Inherits="AllFilesdownloadError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>404 Error</h2>
    <div>
        <p><strong>HTTP 404.</strong> The resource you are looking for </p>
        
        <p>
            (one of its dependencies) could have been removed, had its name changed, or is temporarily unavailable. 
             Please review the following URL and make sure that it is spelled correctly. 
        </p>

    </div>
</asp:Content>

