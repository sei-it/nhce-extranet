﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using ICSharpCode.SharpZipLib.Zip;
using System.Configuration;



public partial class PremonitoringDocs : System.Web.UI.Page
{
    int StateID;
    int PMonitoringEval = 1;
    int PStateLocalAgencyPR = 2;
    int PFiscalOversight=3;
    int PMcKinneyMonitoringEval=4;
    int PMcKinneyVentoProgS=5;
    int PMcKinneyVentoFiscalO = 6;
    int i;
    string OriginalFileName;
    bool valchkbox = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["StateID"] != null)
            {
                StateID = Convert.ToInt32(Page.Request.QueryString["StateID"]);               
            }           
        }

       // loadrecords();
        loadGridview1(StateID);
        loadGridview2(StateID);
        loadGridview3(StateID);
        loadGridview4(StateID);
        loadGridview5(StateID);
        loadGridview6(StateID);      

        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void loadrecords()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qry3 = from e in db.Pre_MonitoringDocLists
                       select e;
          //  chkPremonitoring.DataSource = qry3;
           // chkPremonitoring.DataTextField = "PreMonitoringNames";
          //  chkPremonitoring.DataValueField = "Pre_MonitoringID";
          //  chkPremonitoring.DataBind();
        }
    }   
    //-------------------------------
    private void loadGridview6(int StateID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oAU = from ans in db.PremonitoringUploads
                      where ans.StateID_fk == StateID && ans.SubPremonitoring == PMcKinneyVentoFiscalO
                      select ans;

            GridView6.DataSource = oAU;
            GridView6.DataBind();

        }
    }
    private void loadGridview5(int StateID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oAU = from ans in db.PremonitoringUploads
                      where  ans.StateID_fk == StateID && ans.SubPremonitoring == PMcKinneyVentoProgS
                      select ans;

            GridView5.DataSource = oAU;
            GridView5.DataBind();

        }
    }
    private void loadGridview4(int StateID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oAU = from ans in db.PremonitoringUploads
                      where  ans.StateID_fk == StateID && ans.SubPremonitoring == PMcKinneyMonitoringEval
                      select ans;

            GridView4.DataSource = oAU;
            GridView4.DataBind();

        }
    }
    private void loadGridview3(int StateID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oAU = from ans in db.PremonitoringUploads
                      where  ans.StateID_fk == StateID && ans.SubPremonitoring == PFiscalOversight
                      select ans;

            GridView3.DataSource = oAU;
            GridView3.DataBind();

        }
    }
    private void loadGridview2(int StateID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oAU = from ans in db.PremonitoringUploads
                      where ans.StateID_fk == StateID && ans.SubPremonitoring == PStateLocalAgencyPR
                      select ans;

            GridView2.DataSource = oAU;
            GridView2.DataBind();

        }
    }
    private void loadGridview1(int StateID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            var oAU = from ans in db.PremonitoringUploads where ans.StateID_fk == StateID && ans.SubPremonitoring == PMonitoringEval
                              select ans;

            GridView1.DataSource = oAU;
            GridView1.DataBind();

        }
       
    }
    //----------------------------------------
    
   //----------------------------------------
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            
                string id = (sender as LinkButton).CommandArgument;
                PremonitoringUpload oAU = (from ans in db.PremonitoringUploads where ans.PremonitoringDocID == Convert.ToInt32(id) select ans).FirstOrDefault();

                          
                //if (File.Exists(Server.MapPath("~/PMonitoringEval/") + oAU.PhysicalFileName))
                //{
                //    File.Delete(Server.MapPath("~/PMonitoringEval/") + oAU.PhysicalFileName);
                //}

                string sourceFile = ConfigurationManager.AppSettings["Premonitoring1.1"].ToString();
                if (File.Exists(sourceFile + oAU.PhysicalFileName))
                {
                    File.Delete(sourceFile + oAU.PhysicalFileName);
                }

               
                if ((oAU != null))
                {
                    db.PremonitoringUploads.DeleteOnSubmit(oAU);
                }
                db.SubmitChanges();
                loadGridview1(StateID);
                Response.Redirect("PremonitoringDocs.aspx?StateID="+ StateID);
         }           
            
     }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            string id = (sender as LinkButton).CommandArgument;
            PremonitoringUpload oAU = (from ans in db.PremonitoringUploads where ans.PremonitoringDocID == Convert.ToInt32(id) select ans).FirstOrDefault();

            //if (File.Exists(Server.MapPath("~/PStateLocalAgencyPR/") + oAU.PhysicalFileName))
            //{
            //    File.Delete(Server.MapPath("~/PStateLocalAgencyPR/") + oAU.PhysicalFileName);
            //}

            string sourceFile = ConfigurationManager.AppSettings["Premonitoring2.1"].ToString();
            if (File.Exists(sourceFile + oAU.PhysicalFileName))
            {
                File.Delete(sourceFile + oAU.PhysicalFileName);
            }


            if ((oAU != null))
            {
                db.PremonitoringUploads.DeleteOnSubmit(oAU);
            }
            db.SubmitChanges();
            loadGridview2(StateID);
            Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
        }

    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            string id = (sender as LinkButton).CommandArgument;
            PremonitoringUpload oAU = (from ans in db.PremonitoringUploads where ans.PremonitoringDocID == Convert.ToInt32(id) select ans).FirstOrDefault();

            //if (File.Exists(Server.MapPath("~/PFiscalOversight/") + oAU.PhysicalFileName))
            //{
            //    File.Delete(Server.MapPath("~/PFiscalOversight/") + oAU.PhysicalFileName);
            //}

            string sourceFile = ConfigurationManager.AppSettings["Premonitoring2.2"].ToString();
            if (File.Exists(sourceFile + oAU.PhysicalFileName))
            {
                File.Delete(sourceFile + oAU.PhysicalFileName);
            }


            if ((oAU != null))
            {
                db.PremonitoringUploads.DeleteOnSubmit(oAU);
            }
            db.SubmitChanges();
            loadGridview3(StateID);
            Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
        }

    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            string id = (sender as LinkButton).CommandArgument;
            PremonitoringUpload oAU = (from ans in db.PremonitoringUploads where ans.PremonitoringDocID == Convert.ToInt32(id) select ans).FirstOrDefault();

            //if (File.Exists(Server.MapPath("~/PMcKinneyMonitoringEval/") + oAU.PhysicalFileName))
            //{
            //    File.Delete(Server.MapPath("~/PMcKinneyMonitoringEval/") + oAU.PhysicalFileName);
            //}

            string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.1"].ToString();
            if (File.Exists(sourceFile + oAU.PhysicalFileName))
            {
                File.Delete(sourceFile + oAU.PhysicalFileName);
            }


            if ((oAU != null))
            {
                db.PremonitoringUploads.DeleteOnSubmit(oAU);
            }
            db.SubmitChanges();
            loadGridview4(StateID);
            Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
        }

    }
    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            string id = (sender as LinkButton).CommandArgument;
            PremonitoringUpload oAU = (from ans in db.PremonitoringUploads where ans.PremonitoringDocID == Convert.ToInt32(id) select ans).FirstOrDefault();

            //if (File.Exists(Server.MapPath("~/PMcKinneyVentoProgS/") + oAU.PhysicalFileName))
            //{
            //    File.Delete(Server.MapPath("~/PMcKinneyVentoProgS/") + oAU.PhysicalFileName);
            //}
            string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.2"].ToString();
            if (File.Exists(sourceFile + oAU.PhysicalFileName))
            {
                File.Delete(sourceFile + oAU.PhysicalFileName);
            }


            if ((oAU != null))
            {
                db.PremonitoringUploads.DeleteOnSubmit(oAU);
            }
            db.SubmitChanges();
            loadGridview5(StateID);
            Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
        }

    }
    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            string id = (sender as LinkButton).CommandArgument;
            PremonitoringUpload oAU = (from ans in db.PremonitoringUploads where ans.PremonitoringDocID == Convert.ToInt32(id) select ans).FirstOrDefault();

            //if (File.Exists(Server.MapPath("~/PMcKinneyVentoFiscalO/") + oAU.PhysicalFileName))
            //{
            //    File.Delete(Server.MapPath("~/PMcKinneyVentoFiscalO/") + oAU.PhysicalFileName);
            //}
            string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.3"].ToString();
            if (File.Exists(sourceFile + oAU.PhysicalFileName))
            {
                File.Delete(sourceFile + oAU.PhysicalFileName);
            }


            if ((oAU != null))
            {
                db.PremonitoringUploads.DeleteOnSubmit(oAU);
            }
            db.SubmitChanges();
            loadGridview6(StateID);
            Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
        }

    }
    //--------------------------------
    protected void btUpload_Click(object sender, EventArgs e)
    {
        //Upload_Fu1();
        //Upload_Fu2();
        //Upload_Fu3();
        //Upload_Fu4();
        //Upload_Fu5();
        //Upload_Fu6();    

    }  
    //------------------------------
    
    //-----------------------------
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["StateID"] != null)))
        {
            StateID = Convert.ToInt32(this.ViewState["StateID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["StateID"] = StateID;
        return (base.SaveViewState());
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("StateDocList.aspx?StateID=" + StateID);
    }

    //----------------------
    

    //---------------------------------
   
    protected void ButbtnPremon_Click(object sender, EventArgs e)
    {
              

        lblErrorMsg.Text = "";
        if (FileUploadPre.HasFile)
        {
            string newFileName = Guid.NewGuid().ToString() + "-" + FileUploadPre.FileName;
            OriginalFileName = FileUploadPre.FileName;


            if (chkPm1.Checked == true)
            {
                valchkbox = true;
                //FileUploadPre.SaveAs(Server.MapPath("~/PMonitoringEval/") + newFileName);
                //string Filepath = "~/PMonitoringEval/" + newFileName;
                string sourceFile = ConfigurationManager.AppSettings["Premonitoring1.1"].ToString();
                FileUploadPre.SaveAs(sourceFile + newFileName);
                string Filepath = sourceFile + newFileName;
                UploadPreMonDocument(Filepath, PMonitoringEval, newFileName, OriginalFileName);
                loadGridview1(StateID);
                //  Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
            }


            if (chkPm2.Checked == true)
            {
                valchkbox = true;
                //FileUploadPre.SaveAs(Server.MapPath("~/PStateLocalAgencyPR/") + newFileName);
                //string Filepath = "~/PStateLocalAgencyPR/" + newFileName;

                string sourceFile = ConfigurationManager.AppSettings["Premonitoring2.1"].ToString();
                FileUploadPre.SaveAs(sourceFile + newFileName);
                string Filepath = sourceFile + newFileName;
                UploadPreMonDocument(Filepath, PStateLocalAgencyPR, newFileName, OriginalFileName);
                loadGridview2(StateID);
                // Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
            }


            if (chkPm3.Checked == true)
            {
                valchkbox = true;
                //FileUploadPre.SaveAs(Server.MapPath("~/PFiscalOversight/") + newFileName);
                //string Filepath = "~/PFiscalOversight/" + newFileName;

                string sourceFile = ConfigurationManager.AppSettings["Premonitoring2.2"].ToString();
                FileUploadPre.SaveAs(sourceFile + newFileName);
                string Filepath = sourceFile + newFileName;

                UploadPreMonDocument(Filepath, PFiscalOversight, newFileName, OriginalFileName);
                loadGridview3(StateID);
                // Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
            }


            if (chkPm4.Checked == true)
            {
                valchkbox = true;
                //FileUploadPre.SaveAs(Server.MapPath("~/PMcKinneyMonitoringEval/") + newFileName);
                //string Filepath = "~/PMcKinneyMonitoringEval/" + newFileName;

                string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.1"].ToString();
                FileUploadPre.SaveAs(sourceFile + newFileName);
                string Filepath = sourceFile + newFileName;


                UploadPreMonDocument(Filepath, PMcKinneyMonitoringEval, newFileName, OriginalFileName);
                loadGridview4(StateID);
                // Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
            }


            if (chkPm5.Checked == true)
            {
                valchkbox = true;
                //FileUploadPre.SaveAs(Server.MapPath("~/PMcKinneyVentoProgS/") + newFileName);
                //string Filepath = "~/PMcKinneyVentoProgS/" + newFileName;

                string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.2"].ToString();
                FileUploadPre.SaveAs(sourceFile + newFileName);
                string Filepath = sourceFile + newFileName;

                UploadPreMonDocument(Filepath, PMcKinneyVentoProgS, newFileName, OriginalFileName);
                loadGridview5(StateID);
                // Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
            }

            if (chkPm6.Checked == true)
            {
                valchkbox = true;
                //FileUploadPre.SaveAs(Server.MapPath("~/PMcKinneyVentoFiscalO/") + newFileName);
                //string Filepath = "~/PMcKinneyVentoFiscalO/" + newFileName;

                string sourceFile = ConfigurationManager.AppSettings["Premonitoring3.3"].ToString();
                FileUploadPre.SaveAs(sourceFile + newFileName);
                string Filepath = sourceFile + newFileName;


                UploadPreMonDocument(Filepath, PMcKinneyVentoFiscalO, newFileName, OriginalFileName);
                loadGridview6(StateID);
                // Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
            }

            if (valchkbox == true)
            {               
                Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
            }
            else
            {
                lblErrorMsg.Text = "Please select at least one checkbox to upload file";
            }
        }
        else
        {
            lblErrorMsg.Text = "No file selected";
        }      
        
}

    private void ZipFilesDownload(string zipFileName, List<ListItem> files)
    {
        //// Here we will create zip file & download    
        Response.ContentType = "application/zip";
        Response.AddHeader("content-disposition", "fileName=" + zipFileName);

        using (var zipStream = new ZipOutputStream(Response.OutputStream))
        {

            foreach (ListItem item in files)
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(item.Value);

                var fileEntry = new ZipEntry(Path.GetFileName(item.Value).Substring(37))
                {
                    Size = fileBytes.Length
                };

                zipStream.PutNextEntry(fileEntry);
                zipStream.Write(fileBytes, 0, fileBytes.Length);
            }

            zipStream.Flush();
            zipStream.Close();
        }

    }

    private void UploadPreMonDocument(string Filepath, int PMonitoringEval, string newFileName, string OriginalFileName)
    {
          using (DataClassesDataContext db = new DataClassesDataContext())
             {
                try
                {                  
                    PremonitoringUpload attachment = (from c in db.PremonitoringUploads select c).FirstOrDefault();
                    attachment = new PremonitoringUpload();
                    attachment.OriginalFileName = OriginalFileName;
                    attachment.PhysicalFileName = newFileName;
                    attachment.CreatedBy = User.Identity.Name.ToString();
                    attachment.CreatedDate = DateTime.Now;
                    attachment.StateID_fk = StateID;
                    attachment.SubPremonitoring = PMonitoringEval;
                    attachment.FilePath = Filepath;
                    attachment.Pre_MonitoringID_FK = PMonitoringEval;
                    db.PremonitoringUploads.InsertOnSubmit(attachment);
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {
                    // ILog Log = LogManager.GetLogger("EventLog");
                    // Log.Fatal("File upload", ex);
                }
            }       
        
    }
    
   
}

