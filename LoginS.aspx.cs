﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security;
using System.Web.Security;

public partial class LoginS : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        

        using (DataClassesDataContext db = new DataClassesDataContext())
        {


            AppUser oAU = (from ans in db.AppUsers where ans.sUserName == UserName.Text.ToString() && ans.Password == Password.Text select ans).FirstOrDefault();

            if ((oAU != null))
            {
                if (oAU.RoleID_FK == 1 || oAU.RoleID_FK == 2)
                {
                    FormsAuthentication.RedirectFromLoginPage(UserName.Text, false);
                    Response.Redirect("AllStateDocFiles.aspx");
                }
                else
                {
                    FormsAuthentication.RedirectFromLoginPage(UserName.Text, false);
                    Response.Redirect("StateDocList.aspx");
                }

            }
            else
            {
                ErrorMess.Text = "Your username and/or password are invalid.";
            }

        }
        
    }
}