﻿<%@ Page Title="LEA Math Assessment" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="All_LEAMathAssessment.aspx.cs" Inherits="All_LEAMathAssessment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
      <h2>LEA Math Assessment
        <asp:Label ID="lblheading" runat="server"></asp:Label></h2>
    <asp:Label ID="divMessage" runat="server" ForeColor="Red"></asp:Label>
    <div id="EnrData" runat="server">
        <p style="text-align: right; font-size: 18px">
            <asp:LinkButton ForeColor="Blue" ID="lnkData" Text="Download LEA Enrolled Data" runat="server" OnClick="lnkData_Click" />
        </p>

        <asp:PlaceHolder ID="PlaceHolder1" Visible="true" runat="server"></asp:PlaceHolder>
    </div>
</asp:Content>

