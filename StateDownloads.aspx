﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="StateDownloads.aspx.cs" Inherits="StateDownloads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
     <h2>Download State Zipped Files</h2>
    <asp:GridView ID="gvDocFiles" runat="server" Width="600px" CssClass="etacTbl"
         AutoGenerateColumns="false" AllowPaging="false" PageSize="15" OnPageIndexChanging="gvDocFiles_PageIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="State List">
                <ItemTemplate>
                    <%--<asp:HyperLink ID="link" runat="server" Text='<%# Eval("StateFullName") %>' />--%>
                    <a href="StateDocList.aspx?StateID=<%#Eval("StateID") %>"><b><%# Eval("StateFullName") %></b></a>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Download" ItemStyle-HorizontalAlign="center">
                <ItemTemplate >
                    <%--<asp:HyperLink ID="link" runat="server" Text='<%# Eval("StateFullName") %>' />--%>
                     <asp:LinkButton ID="LinkButton20" runat="server" Text="Download" class="linka" ForeColor="White"
                            OnClick="LinkButton20_Click"  CommandArgument='<%# Eval("StateID")%>' />
                   
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

