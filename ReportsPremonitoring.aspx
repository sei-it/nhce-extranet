﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReportsPremonitoring.aspx.cs" Inherits="ReportsPremonitoring" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
        <script  lang="javascript">
            function printdiv(printpage) {
                var headstr = "<html><head><title></title></head><body>";
                var footstr = "</body>";
                // var newstr = document.all.item(printpage).innerHTML;
                var newstr = document.getElementById(printpage).innerHTML;
                var oldstr = document.body.innerHTML;
                document.body.innerHTML = headstr + newstr + footstr;
                window.print();
                document.body.innerHTML = oldstr;
                return false;
            }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" Runat="Server">
<div id="print">
    <h1>Premonitoring Sub category Reports</h1>
   
    <asp:GridView ID="gvDocFiles" runat="server" Width="600px" CssClass="etacTbl" OnRowDataBound="gvDocFiles_RowDataBound"
                AutoGenerateColumns="false" AllowPaging="false" >

        <Columns>

            <asp:TemplateField>
                    <ItemTemplate>
                      <asp:ImageButton ID="imageCheck" runat="server" Width="8px" Height="8px" ImageUrl="Images/check.gif" Enabled="false" Visible="false" />
                    </ItemTemplate>
                </asp:TemplateField> 

            <asp:TemplateField HeaderText="State List">
                <ItemTemplate>                    
                    <a href="StateDocList.aspx?StateID=<%#Eval("StateID") %>"><b><%# Eval("StateFullName") %></b></a>
                </ItemTemplate>
            </asp:TemplateField>
           

            <asp:TemplateField HeaderText="Premonitoring 1.1">
                    <ItemTemplate>                      
                         <asp:Label ID="Label1" runat="server" Text='<%# Eval("_1") %>'></asp:Label>
                    </ItemTemplate>                    
                </asp:TemplateField>

             <asp:TemplateField HeaderText="Premonitoring 2.1">
                    <ItemTemplate>                       
                         <asp:Label ID="Label2" runat="server" Style="align-content:center" Text='<%# Eval("_2") %>'></asp:Label>
                    </ItemTemplate>                    
                </asp:TemplateField>


             <asp:TemplateField HeaderText="Premonitoring 2.2">
                    <ItemTemplate>
                       
                        <%-- <asp:Label ID="Label3" runat="server" Text='<%# Eval("PremonitoringDocID")=="1" ? true: false %>'></asp:Label>--%>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("_3") %>'></asp:Label>
                    </ItemTemplate>                    
                </asp:TemplateField>

            <asp:TemplateField HeaderText="Premonitoring 3.1">
                    <ItemTemplate>
                       
                       
                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("_4") %>'></asp:Label>
                    </ItemTemplate>                    
                </asp:TemplateField>

             <asp:TemplateField HeaderText="Premonitoring 3.2 ">
                    <ItemTemplate>
                       
                       
                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("_5") %>'></asp:Label>
                    </ItemTemplate>                    
                </asp:TemplateField>


            <asp:TemplateField HeaderText="Premonitoring 3.3">
                    <ItemTemplate>
                       
                       
                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("_6") %>'></asp:Label>
                    </ItemTemplate>                    
                </asp:TemplateField>
            

        </Columns>
    </asp:GridView>
</div>

     <p><asp:Button ID="btnPrint" class="rotate printme" runat="server" Text="" OnClientClick="printdiv('print');" /></p>


</asp:Content>

