﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;

public partial class Math_PreFilledReport : System.Web.UI.Page
{
    string Year = "";
    string StateName = "";
    string sName;
    string sYear;   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Page.Request.QueryString["Year"] != null)
            {
                Year = Page.Request.QueryString["Year"].ToString();
            }
            if (Page.Request.QueryString["StateName"] != null)
            {
                StateName = Page.Request.QueryString["StateName"].ToString();
            }            

        }
        DisplayMath();

        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void DisplayMath()
    {
        ReportData rep1 = new ReportData();
        DataSet dsEnrolled = new DataSet();

        // EnrData.Visible = true;
        divMessage.Text = "";
        dsEnrolled = rep1.Get_AllPrefilled_Math(StateName, Year, "spMath_PercentGrade");
        //   dsEnrolled = rep1.Math(SchoolYear, "Math_EnrollmentSCount_SY11_14");

        if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
        {
            grdVwList.DataSource = dsEnrolled;
            grdVwList.DataBind();
        }
               

    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["Year"] != null)))
        {
            Year = this.ViewState["Year"].ToString();
        }
        if (((this.ViewState["StateName"] != null)))
        {
            StateName = this.ViewState["StateName"].ToString();
        }

    }
    protected override object SaveViewState()
    {
        this.ViewState["Year"] = Year;
        this.ViewState["StateName"] = StateName;
        return (base.SaveViewState());
    }

   
    //generate excel report 
    protected void lnkData_Click(object sender, EventArgs e)
    {
       
        Get_ExcelData();
    }
    private void Get_ExcelData()
    {

        ReportData rep1 = new ReportData();
        DataSet dsEnrolled = new DataSet();


        // EnrData.Visible = true;
        divMessage.Text = "";
        dsEnrolled = rep1.Get_AllPrefilled_Math(StateName, Year, "spMath_PercentGrade");


        if (dsEnrolled != null && dsEnrolled.Tables[0].Rows.Count > 0)
        {
            ExportToExcel(dsEnrolled, 0, Response, "PreFilledReport");
        }


    }

    // excel format
    private void ExportToExcel(DataSet ds, int TableIndex, HttpResponse Response, string FileName)
    {
        int i, j;
        DataTable dt = new DataTable();
        dt = ds.Tables[0];

        // Response.ContentType = "application/vnd.ms-excel";
        string timestamp = Convert.ToString(DateTime.Now);
        timestamp = timestamp.Replace(" ", "_");
        timestamp = timestamp.Replace("/", "_");
        //  timestamp = timestamp.Replace(":", ":");
        FileName = FileName.Replace(" ", "");

        //string ExtractName = "Math" + "(" + timestamp + ")" + FileName + " .xlsx";
        string ExtractName = "Math_" + FileName + " .xlsx";
        //  Response.AppendHeader("content-disposition", "attachment; filename=" + ExtractName + ".xls");

        string sTempFileName = Server.MapPath(@"~\temp\dataeX_") + Guid.NewGuid().ToString() + ".xlsx";

        ExportDataSet(ds, sTempFileName);
        displayExport(sTempFileName, ExtractName);

    }
    public static void ExportDataSet(DataSet ds, string destination)
    {
        using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
        {
            var workbookPart = workbook.AddWorkbookPart();

            workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

            workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();

            foreach (System.Data.DataTable table in ds.Tables)
            {

                var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                uint sheetId = 1;
                if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                {
                    sheetId =
                        sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                }

                DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = table.TableName };
                sheets.Append(sheet);

                DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                List<String> columns = new List<string>();
                foreach (System.Data.DataColumn column in table.Columns)
                {
                    columns.Add(column.ColumnName);

                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                    cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName);
                    headerRow.AppendChild(cell);
                }


                sheetData.AppendChild(headerRow);

                foreach (System.Data.DataRow dsrow in table.Rows)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (String col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();

                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        dynamic dc = dsrow[col];
                        if (dc.GetType().ToString() == "System.Boolean")
                        {
                            if (dsrow[col].ToString() == "True")
                            {
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Yes");
                            }

                        }
                        else
                        {
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow[col].ToString());
                        }
                        //if (Type.GetType(dc.GetType().ToString()))
                        //
                        newRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(newRow);
                }

            }
        }
    }
    private void displayExport(string sTempFileName, string sFilename)
    {
        try
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.spreadsheet";
            Response.AddHeader("Content-Disposition", "inline; filename=\"" + sFilename + "\"");
            Response.Flush();
            byte[] databyte = File.ReadAllBytes(sTempFileName);
            MemoryStream ms = new MemoryStream();
            ms.Write(databyte, 0, databyte.Length);
            ms.Position = 0;
            ms.Capacity = Convert.ToInt32(ms.Length);
            byte[] arrbyte = ms.GetBuffer();
            ms.Close();
            Response.BinaryWrite(arrbyte);

            Response.End();

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (File.Exists(sTempFileName))
            {
                File.Delete(sTempFileName);

            }
        }
    }
}