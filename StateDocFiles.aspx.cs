﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class StateDocFiles : System.Web.UI.Page
{
    int intFileID;
   
    protected void Page_Load(object sender, EventArgs e)
    {

       
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["FileID"] != null)
            {
                intFileID = Convert.ToInt32(Page.Request.QueryString["FileID"]);
                if (IsFIleAllowed(intFileID))
                {
                displayDocument(intFileID);
                }
            }

        }
    }
    private bool IsFIleAllowed(int FileID)
    {
        bool blIsValid = false;
         using (DataClassesDataContext db = new DataClassesDataContext())
            {

                AppUser oAU = (from ans in db.AppUsers where ans.sUserName == User.Identity.Name.ToString() select ans).FirstOrDefault();
                var oFileRecs = db.IsFileAllowedForUser(User.Identity.Name.ToString(), FileID).ToList();


                if (oAU.RoleID_FK == 1 || oAU.RoleID_FK == 2)
                {
                    blIsValid = true;
                }
                else
                {

                    foreach (var oEBP in oFileRecs)
                    {
                        if (oEBP.Column1 > 0)
                        {
                            blIsValid = true;
                        }
                        else
                        {
                            blIsValid = false;
                        }
                    }
                }         
            }

        return blIsValid;
    }
    private void displayDocument(int FileID)
    {
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oApps = from pg in db.StateDocs
                        where pg.StateDocID == FileID
                        select pg;
            foreach (var oApp in oApps)
            {



                objVal = oApp.FileName;
                if (objVal != null)
                {
                    displayDocument01(objVal.ToString());
                }

            }

        }
    }

    private void displayDocument01(string strFileName)
    {
        //   Dim strRequest As String = Request.QueryString("file") '-- if something was passed to the file querystring
        //get absolute path of the file
        if (!string.IsNullOrEmpty(strFileName))
        {
            NameValueCollection appSettings = System.Configuration.ConfigurationManager.AppSettings;
            //string path = appSettings["PanelFilePath"] + strFileName + "";

            //get file object as FileInfo
           // System.IO.FileInfo file = new System.IO.FileInfo(path);
            string sourceFile = ConfigurationManager.AppSettings["AppFilePath"].ToString();
            string Filepath = sourceFile + strFileName;
            System.IO.FileInfo file = new System.IO.FileInfo(Filepath);
            //-- if the file exists on the server
            //set appropriate headers
            if (file.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "inline; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                string sExtn;
                sExtn = file.Extension;
                switch (sExtn)
                {
                    case ".pdf":
                        Response.ContentType = "application/pdf";
                        break;
                    case ".xls":
                        Response.ContentType = "application/excel";
                        break;
                    case ".xlsx":
                        Response.ContentType = "application/excel";
                        break;
                    case ".doc":
                        Response.ContentType = "application/msword";
                        break;
                    case "docx":
                        Response.ContentType = "application/msword";
                        break;
                    default:
                        Response.ContentType = "application/octet-stream";
                        break;


                }
                Response.WriteFile(file.FullName);
                Response.End();
                //if file does not exist
            }
            else
            {
                //LiteralMessage.Text = "This file does not exist.";
            }
            //nothing in the URL as HTTP GET
        }
        else
        {
            // LiteralMessage.Text = "This file does not exist.";
        }
    }
}