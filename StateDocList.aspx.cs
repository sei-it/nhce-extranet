﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Web.Security;


public partial class StateDocList : System.Web.UI.Page
{
    int StateID;
    int Roleid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["StateID"] != null)
            {
                StateID = Convert.ToInt32(Page.Request.QueryString["StateID"]);                
            }
           
            displayStatus();
            LoadStateName(StateID);
        }       

    }

    private void LoadStateName(int StateID)
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {

            State oAU = (from ans in db.States
                         where ans.StateID == StateID
                           select ans).FirstOrDefault();

            if ((oAU != null))
            {
                lbStateName.Text = Convert.ToString(oAU.StateFullName);
            }
        }
    }
    protected void displayStatus()
    {

        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
           
            AppUser oAU = (from ans in db.AppUsers
                           where ans.sUserName == User.Identity.Name.ToString()
                           select ans).FirstOrDefault();

            if ((oAU != null))
            {
                if (oAU.RoleID_FK == 1 || oAU.RoleID_FK == 2)
                {
                    var oApps = from pg in db.StateDocs
                                where pg.StateID_fk == StateID
                                select pg;
                    foreach (var oApp in oApps)
                    {
                        objVal = oApp.FileName;
                        if (objVal != null)
                        {
                            gvDocFiles.DataSource = oApps;
                            gvDocFiles.DataBind();
                        }
                    }
                }
                else
                {
                    StateID = Convert.ToInt32(oAU.StateID_FK);
                    var oQnTOC = db.getFilesForStateCordinator(User.Identity.Name.ToString()).ToList();
                    gvDocFiles.DataSource = oQnTOC;
                    gvDocFiles.DataBind();
                }
            }
        }
    }

    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["StateID"] != null)))
        {
            StateID = Convert.ToInt32(this.ViewState["StateID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["StateID"] = StateID;        
        return (base.SaveViewState());
    }
    protected void lnkPremonitiring_Click(object sender, EventArgs e)
    {
        Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
    }
    protected void lnkWorkbook_Click(object sender, EventArgs e)
    {
        Response.Redirect("DataWorkbooks.aspx?StateID=" + StateID);
    }
    protected void lnkStateAction_Click(object sender, EventArgs e)
    {
         Response.Redirect("StateActionPlans.aspx?StateID=" + StateID);
       
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("PremonitoringDocs.aspx?StateID=" + StateID);
    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        Response.Redirect("StateActionPlans.aspx?StateID=" + StateID);
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        Response.Redirect("DataWorkbooks.aspx?StateID=" + StateID);
       
    }
}