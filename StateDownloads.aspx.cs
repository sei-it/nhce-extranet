﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Checksums;
using System.Threading;

using System.Configuration;


public partial class StateDownloads : System.Web.UI.Page
{
    string preId;
    string[] filePaths1;
    string[] filePaths2;
    string[] filePaths3;
    string[] filePaths4;
    string[] filePaths5;
    string[] filePaths6;
    string[] filePaths7;
    string[] filePaths8;
    ZipOutputStream zos;
    string strBaseDir;
    string Foldername;
    string sFullName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                using (DataClassesDataContext db = new DataClassesDataContext())
                {
                    AppUser oAU = (from ans in db.AppUsers
                                   where ans.sUserName == Context.User.Identity.Name.ToString()
                                   select ans).FirstOrDefault();

                    if ((oAU != null))
                    {
                        if (!(oAU.RoleID_FK == 1 || oAU.RoleID_FK == 2))
                        {                            
                           Response.Redirect("AllFilesdownloadError.aspx");
                        }
                        displayStatus();
                    }
                }

            }
        }
    }
    protected void displayStatus()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            BindData();

        }
    }

    private void BindData()
    {
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oQnTOC = (from m in db.States
                          where m.StateFullName != null
                          orderby m.StateFullName
                          select m).ToList();
            gvDocFiles.DataSource = oQnTOC;
            gvDocFiles.DataBind();
        }
    }
    protected void gvDocFiles_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDocFiles.PageIndex = e.NewPageIndex;
        BindData();
    }
    protected void LinkButton20_Click(object sender, EventArgs e)
    {
        List<ListItem> files = new List<ListItem>();

        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            string id = (sender as LinkButton).CommandArgument;


            // get StateName
            var oQnTOC = from m in db.States
                          where  m.StateID == Convert.ToInt32(id) && m.StateFullName!=null
                          select m;
            foreach (var sName in oQnTOC)
            {
                 if (sName.StateFullName != null)
                {
                    sFullName = sName.StateFullName; 
                 }
            }


            // type 1
            string sourceFile1 = ConfigurationManager.AppSettings["Premonitoring1.1"].ToString();
            Foldername = ConfigurationManager.AppSettings["UserFolders"].ToString();
            if (sourceFile1 != null)
            {
                string Premonitoring_MonitoringEvaluation_LEAs = "Premonitoring_MonitoringEvaluation_LEAs";
                string pathToCreate = "~/UserFolders/" + Premonitoring_MonitoringEvaluation_LEAs;
               // string pathToCreate = "~/" + Foldername + Premonitoring_MonitoringEvaluation_LEAs;
                
                if (Directory.Exists(Server.MapPath(pathToCreate)))
                {
                    DeleteDirectory(Server.MapPath(pathToCreate));
                }
                //Now you know it is ok, create itDele               
                Directory.CreateDirectory(Server.MapPath(pathToCreate));

                preId = "1";
                var oAU = from ans in db.PremonitoringUploads
                          where ans.StateID_fk == Convert.ToInt32(id) && ans.SubPremonitoring == Convert.ToInt32(preId)
                          select ans;


                filePaths1 = Directory.GetFiles(sourceFile1);
                string folder_path = Server.MapPath(pathToCreate +"/");

                foreach (string filePath in filePaths1)
                {
                    foreach (var f in oAU)
                    {
                        if (f.PhysicalFileName == Path.GetFileName(filePath))
                        {
                            FileInfo fi2 = new FileInfo(filePath);
                            if (fi2.Exists)
                            {
                                // move it to the folder
                                try
                                {
                                    fi2.CopyTo(folder_path + fi2.Name.Substring(37), true);
                                }
                                catch
                                {
                                    // clean up if the operation failed
                                    System.IO.Directory.Delete(folder_path);
                                    return;
                                }
                            }
                        }
                    }
                }
            }


       // type 2

        string sourceFile2 = ConfigurationManager.AppSettings["Premonitoring2.1"].ToString();
        Foldername = ConfigurationManager.AppSettings["UserFolders"].ToString();

        if (sourceFile2 != null)
        {
            string Premonitoring_IdentificationEnrollmentRetention = "Premonitoring_IdentificationEnrollmentRetention";
              string pathToCreate = "~/UserFolders/" + Premonitoring_IdentificationEnrollmentRetention;
           // string varPath = Foldername + "Premonitoring_IdentificationEnrollmentRetention";
           // string pathToCreate = "~/" + Foldername + Premonitoring_IdentificationEnrollmentRetention;
          //  string pathToCreate = @"varPath";
            if (Directory.Exists(Server.MapPath(pathToCreate)))
            {
                DeleteDirectory(Server.MapPath(pathToCreate));
            }
            //Now you know it is ok, create itDele               
            Directory.CreateDirectory(Server.MapPath(pathToCreate));
            preId = "2";
            var oAU = from ans in db.PremonitoringUploads
                      where ans.StateID_fk == Convert.ToInt32(id) && ans.SubPremonitoring == Convert.ToInt32(preId)
                      select ans;

            filePaths2 = Directory.GetFiles(sourceFile2);
            string folder_path = Server.MapPath(pathToCreate + "/");

            foreach (string filePath in filePaths2)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        FileInfo fi2 = new FileInfo(filePath);
                        if (fi2.Exists)
                        {
                            // move it to the folder
                            try
                            {
                                fi2.CopyTo(folder_path + fi2.Name.Substring(37), true);
                            }
                            catch
                            {
                                // clean up if the operation failed
                                System.IO.Directory.Delete(folder_path);
                                return;
                            }
                        }
                    }
                }
            }
        }

        // type 3
        string sourceFile3 = ConfigurationManager.AppSettings["Premonitoring2.2"].ToString();
        Foldername = ConfigurationManager.AppSettings["UserFolders"].ToString();

        if (sourceFile3 != null)
        {
            string Premonitoring_TechnicalAssistance_LEAs = "Premonitoring_TechnicalAssistance_LEAs";
            string pathToCreate = "~/UserFolders/" + Premonitoring_TechnicalAssistance_LEAs;
           // string pathToCreate = "~/" + Foldername + Premonitoring_TechnicalAssistance_LEAs;

           
            if (Directory.Exists(Server.MapPath(pathToCreate)))
            {
                DeleteDirectory(Server.MapPath(pathToCreate));
            }
            //Now you know it is ok, create itDele               
            Directory.CreateDirectory(Server.MapPath(pathToCreate));
            preId = "3";
            var oAU = from ans in db.PremonitoringUploads
                      where ans.StateID_fk == Convert.ToInt32(id) && ans.SubPremonitoring == Convert.ToInt32(preId)
                      select ans;

            filePaths3 = Directory.GetFiles(sourceFile3);
            string folder_path = Server.MapPath(pathToCreate + "/");

            foreach (string filePath in filePaths3)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        FileInfo fi2 = new FileInfo(filePath);
                        if (fi2.Exists)
                        {
                            // move it to the folder
                            try
                            {
                                fi2.CopyTo(folder_path + fi2.Name.Substring(37), true);
                            }
                            catch
                            {
                                // clean up if the operation failed
                                System.IO.Directory.Delete(folder_path);
                                return;
                            }
                        }
                    }
                }
            }
        }

        //type 4

        string sourceFile4 = ConfigurationManager.AppSettings["Premonitoring3.1"].ToString();
        Foldername = ConfigurationManager.AppSettings["UserFolders"].ToString();

        if (sourceFile4 != null)
        {
            string Premonitoring_LEA_Subgrants = "Premonitoring_LEA_Subgrants";
            string pathToCreate = "~/UserFolders/" + Premonitoring_LEA_Subgrants;
           // string pathToCreate = "~/" + Foldername + Premonitoring_LEA_Subgrants;

          
            if (Directory.Exists(Server.MapPath(pathToCreate)))
            {
                DeleteDirectory(Server.MapPath(pathToCreate));
            }
            //Now you know it is ok, create itDele               
            Directory.CreateDirectory(Server.MapPath(pathToCreate));
            preId = "4";
            var oAU = from ans in db.PremonitoringUploads
                      where ans.StateID_fk == Convert.ToInt32(id) && ans.SubPremonitoring == Convert.ToInt32(preId)
                      select ans;

            filePaths4 = Directory.GetFiles(sourceFile4);
            string folder_path = Server.MapPath(pathToCreate + "/");

            foreach (string filePath in filePaths4)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        FileInfo fi2 = new FileInfo(filePath);
                        if (fi2.Exists)
                        {
                            // move it to the folder
                            try
                            {
                                fi2.CopyTo(folder_path + fi2.Name.Substring(37), true);
                            }
                            catch
                            {
                                // clean up if the operation failed
                                System.IO.Directory.Delete(folder_path);
                                return;
                            }
                        }
                    }
                }
            }
        }

        //-----type 5
        string sourceFile5 = ConfigurationManager.AppSettings["Premonitoring3.2"].ToString();
        Foldername = ConfigurationManager.AppSettings["UserFolders"].ToString();

        if (sourceFile5 != null)
        {
            string Premonitoring_Statelevel_Coordination = "Premonitoring_Statelevel_Coordination";
            string pathToCreate = "~/UserFolders/" + Premonitoring_Statelevel_Coordination;
           // string pathToCreate = "~/" + Foldername + Premonitoring_Statelevel_Coordination;
            
            if (Directory.Exists(Server.MapPath(pathToCreate)))
            {
                DeleteDirectory(Server.MapPath(pathToCreate));
            }
            //Now you know it is ok, create itDele               
            Directory.CreateDirectory(Server.MapPath(pathToCreate));
            preId = "5";
            var oAU = from ans in db.PremonitoringUploads
                      where ans.StateID_fk == Convert.ToInt32(id) && ans.SubPremonitoring == Convert.ToInt32(preId)
                      select ans;

            filePaths5 = Directory.GetFiles(sourceFile5);
            string folder_path = Server.MapPath(pathToCreate + "/");

            foreach (string filePath in filePaths5)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        FileInfo fi2 = new FileInfo(filePath);
                        if (fi2.Exists)
                        {
                            // move it to the folder
                            try
                            {
                                fi2.CopyTo(folder_path + fi2.Name.Substring(37), true);
                            }
                            catch
                            {
                                // clean up if the operation failed
                                System.IO.Directory.Delete(folder_path);
                                return;
                            }
                        }
                    }
                }
            }
        }

        //---type 6
        string sourceFile6 = ConfigurationManager.AppSettings["Premonitoring3.3"].ToString();
        Foldername = ConfigurationManager.AppSettings["UserFolders"].ToString();

        if (sourceFile6 != null)
        {
            string Premonitoring_DisputeResolution = "Premonitoring_DisputeResolution";
            string pathToCreate = "~/UserFolders/" + Premonitoring_DisputeResolution;
            //string pathToCreate = "~/" + Foldername + Premonitoring_DisputeResolution;
            
            if (Directory.Exists(Server.MapPath(pathToCreate)))
            {
                DeleteDirectory(Server.MapPath(pathToCreate));
            }
            //Now you know it is ok, create itDele               
            Directory.CreateDirectory(Server.MapPath(pathToCreate));
            preId = "6";
            var oAU = from ans in db.PremonitoringUploads
                      where ans.StateID_fk == Convert.ToInt32(id) && ans.SubPremonitoring == Convert.ToInt32(preId)
                      select ans;

            filePaths6 = Directory.GetFiles(sourceFile6);
            string folder_path = Server.MapPath(pathToCreate + "/");

            foreach (string filePath in filePaths6)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        FileInfo fi2 = new FileInfo(filePath);
                        if (fi2.Exists)
                        {
                            // move it to the folder
                            try
                            {
                                fi2.CopyTo(folder_path + fi2.Name.Substring(37), true);
                            }
                            catch
                            {
                                // clean up if the operation failed
                                System.IO.Directory.Delete(folder_path);
                                return;
                            }
                        }
                    }
                }
            }
        }

        // type Data Workbooks
        string sourceFile7 = ConfigurationManager.AppSettings["DataWorkbooks"].ToString();
        Foldername = ConfigurationManager.AppSettings["UserFolders"].ToString();   

        if (sourceFile7 != null)
        {
            string DataWorkbooks = "DataWorkbooks";
            string pathToCreate = "~/UserFolders/" + DataWorkbooks;
           // string pathToCreate = "~/" + Foldername + DataWorkbooks;
           
            if (Directory.Exists(Server.MapPath(pathToCreate)))
            {
                DeleteDirectory(Server.MapPath(pathToCreate));
            }
            //Now you know it is ok, create itDele               
            Directory.CreateDirectory(Server.MapPath(pathToCreate));
           // preId = "6";
            var oAU = from ans in db.StateWorkbookUploads
                      where ans.StateID_fk == Convert.ToInt32(id) 
                      select ans;

            filePaths7 = Directory.GetFiles(sourceFile7);
            string folder_path = Server.MapPath(pathToCreate + "/");

            foreach (string filePath in filePaths7)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        FileInfo fi2 = new FileInfo(filePath);
                        if (fi2.Exists)
                        {
                            // move it to the folder
                            try
                            {
                                fi2.CopyTo(folder_path + fi2.Name.Substring(37), true);
                            }
                            catch
                            {
                                // clean up if the operation failed
                                System.IO.Directory.Delete(folder_path);
                                return;
                            }
                        }
                    }
                }
            }
        }

        // type State Action Plans
        string sourceFile8 = ConfigurationManager.AppSettings["StateAction"].ToString();
        Foldername = ConfigurationManager.AppSettings["UserFolders"].ToString();
        if (sourceFile8 != null)
        {
            string StateAction = "StateAction";
            string pathToCreate = "~/UserFolders/" + StateAction;
           // string pathToCreate = "~/" + Foldername + StateAction;

           
            if (Directory.Exists(Server.MapPath(pathToCreate)))
            {
                DeleteDirectory(Server.MapPath(pathToCreate));
            }
            //Now you know it is ok, create itDele               
            Directory.CreateDirectory(Server.MapPath(pathToCreate));
            // preId = "6";
            var oAU = from ans in db.StateActionPlansUploads
                      where ans.StateID_fk == Convert.ToInt32(id)
                      select ans;

            filePaths8 = Directory.GetFiles(sourceFile8);
            string folder_path = Server.MapPath(pathToCreate + "/");

            foreach (string filePath in filePaths8)
            {
                foreach (var f in oAU)
                {
                    if (f.PhysicalFileName == Path.GetFileName(filePath))
                    {
                        FileInfo fi2 = new FileInfo(filePath);
                        if (fi2.Exists)
                        {
                            // move it to the folder
                            try
                            {
                                fi2.CopyTo(folder_path + fi2.Name.Substring(37), true);
                            }
                            catch
                            {
                                // clean up if the operation failed
                                System.IO.Directory.Delete(folder_path);
                                return;
                            }
                        }
                    }
                }
            }
        }

      // zip entire directorty

        string UserFolders = ConfigurationManager.AppSettings["UserFolders"].ToString();
        StartZip(Server.MapPath("UserFolders"), sFullName + "_Uploadedfiles");

        } //---end of db
    } // ---end of function

    protected void StartZip(string strPath, string strFileName)
    {
        MemoryStream ms = null;
        Response.ContentType = "application/octet-stream";
        strFileName = HttpUtility.UrlEncode(strFileName).Replace('+', ' ');
        Response.AddHeader("Content-Disposition", "attachment; filename=" + strFileName + ".zip");
        ms = new MemoryStream();
        zos = new ZipOutputStream(ms);
        strBaseDir = strPath + "\\";
        addZipEntry(strBaseDir);
        zos.Finish();
        zos.Close();
        Response.Clear();
        Response.BinaryWrite(ms.ToArray());
        Response.End();
    }
    protected void addZipEntry(string PathStr)
    {
        DirectoryInfo di = new DirectoryInfo(PathStr);
        foreach (DirectoryInfo item in di.GetDirectories())
        {
            addZipEntry(item.FullName);
        }
        foreach (FileInfo item in di.GetFiles())
        {
            FileStream fs = File.OpenRead(item.FullName);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            string strEntryName = item.FullName.Replace(strBaseDir, "");
            ZipEntry entry = new ZipEntry(strEntryName);
            zos.PutNextEntry(entry);
            zos.Write(buffer, 0, buffer.Length);
            fs.Close();
        }
    }


    public static void DeleteDirectory(string target_dir)
    {
        string[] files = Directory.GetFiles(target_dir);
        string[] dirs = Directory.GetDirectories(target_dir);

        foreach (string file in files)
        {
            File.SetAttributes(file, FileAttributes.Normal);
            File.Delete(file);
        }

        foreach (string dir in dirs)
        {
            DeleteDirectory(dir);
        }

        Directory.Delete(target_dir, false);
    }
    private void ZipFilesDownload(string zipFileName, List<ListItem> files)
    {
        //// Here we will create zip file & download    
        Response.ContentType = "application/zip";
        Response.AddHeader("content-disposition", "fileName=" + zipFileName);

        using (var zipStream = new ZipOutputStream(Response.OutputStream))
        {

            foreach (ListItem item in files)
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(item.Value);

                var fileEntry = new ZipEntry(Path.GetFileName(item.Value).Substring(37))
                {
                    Size = fileBytes.Length
                };

                zipStream.PutNextEntry(fileEntry);
                zipStream.Write(fileBytes, 0, fileBytes.Length);
            }

            zipStream.Flush();
            zipStream.Close();
        }

    }
            
}