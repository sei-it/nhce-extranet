﻿<%@ Page Title="SEA data" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SEAdata.aspx.cs" Inherits="SEAdata" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <h2>SEA CSPR Longitudinal Data</h2>
    <div>
        <p style="font-size: 15px">
            <asp:HyperLink NavigateUrl="SEA_Enrollment-1.aspx" runat="server" Visible="true" ID="LinkEnr" ForeColor="Blue" Text="Enrolled SEA School Data"></asp:HyperLink></p>
        <p style="font-size: 15px">
            <asp:HyperLink NavigateUrl="SEA_Served-1.aspx" runat="server" Visible="true" ID="LinSer" ForeColor="Blue" Text="Served SEA School Data"></asp:HyperLink></p>
        <p style="font-size: 15px">
            <asp:HyperLink NavigateUrl="SEA_MathAssessment.aspx" runat="server" Visible="true" ID="lnkMath" ForeColor="Blue" Text="Math Assessment SEA Data"></asp:HyperLink></p>
        <p style="font-size: 15px">
            <asp:HyperLink NavigateUrl="SEA_ScienceAssessment.aspx" runat="server" Visible="true" ID="LinScie" ForeColor="Blue" Text="Science Assessment SEA Data"></asp:HyperLink></p>
        <p style="font-size: 15px">
            <asp:HyperLink NavigateUrl="SEA_EnglishAssessment.aspx" runat="server" Visible="true" ID="LinEng" ForeColor="Blue" Text="ELA Assessment SEA Data"></asp:HyperLink></p>
    </div>
   <br />
    <div>
        <h2>PRE-FILLED REPORTS</h2>

        <div class="colx col1">
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click"><img class="premonitoring hvr-pop" src="Images/document_icon.png" alt="TotalHomeless Documents">
   <h2>Total Homeless Student Enrollment</h2></asp:LinkButton>
            <p>Report 1:  Comparison of Homeless Children and Youth Enrolled in Pre-Kindergarten through Grade 13 and Proportion of People Under the Age of 18 Living Below the Poverty Line</p>
            <asp:LinkButton ForeColor="Blue" ID="lnkTotalHomeless" Text="Report 1" runat="server" OnClick="lnkTotalHomeless_Click" />
        </div>
        <div class="colx col2">
            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click"> <img class="premonitoring hvr-pop"  src="Images/grad_icon.png" alt="Premonitoring Documents">
    <h2>Homeless Graduation Rates</h2></asp:LinkButton>
            <p>Report 2: This report displays the Homeless Graduation Rates Data </p>
            <asp:LinkButton ForeColor="Blue" ID="lnkHomelessGraduation" Text="Report 2" runat="server" OnClick="lnkHomelessGraduation_Click"/>
        </div>

        <div class="colx col3">
            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="LinkButton3_Click"> <img class="premonitoring hvr-pop" src="Images/workbooks_icon.png" alt="Premonitoring Documents">
     <h2>Number of HCY Proficient in Mathematics</h2></asp:LinkButton>
            <p>Report 3: Proportion of Homeless Children and Youth Assigned a Valid Score on Mathematics Assessments who were Proficient</p>
            <asp:LinkButton ForeColor="Blue" ID="lnkHCYProfMathematics" Text="Report 3" runat="server" OnClick="lnkHCYProfMathematics_Click" />
        </div>

        <div class="colx col4">
            <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click"> <img class="premonitoring hvr-pop" src="Images/action_icon.png"   alt="Premonitoring Documents">
     <h2>Number of HCY Proficient in ELA</h2></asp:LinkButton>
            <p>Report 4:  Proportion of Homeless Children and Youth Assigned a Valid Score on English/Language Arts Assessments who were Proficient</p>
            <asp:LinkButton ForeColor="Blue" ID="InkHCYProfELA" Text="Report 4" runat="server" OnClick="InkHCYProfELA_Click" />
        </div>
    </div>

</asp:Content>

